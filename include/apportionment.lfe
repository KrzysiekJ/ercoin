(deftype result (tuple (any) (pos_integer)))
(deftype score (tuple (non_neg_integer) (any)))
