# Licensed under the Apache License, Version 2.0 (the “License”);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an “AS IS” BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PROJECT = ercoin
PROJECT_DESCRIPTION = A simple cryptocurrency using Tendermint
PROJECT_VERSION = 0.1.0

DEPS = abci_server dynarec erlsha2 gb_merkle_trees jiffy libsodium nist_beacon triq
dep_abci_server = git https://github.com/KrzysiekJ/abci_server.git v0.4.0
dep_dynarec = git https://github.com/dieswaytoofast/dynarec.git 1f477
dep_erlsha2 = git https://github.com/vinoski/erlsha2 2.2.1
dep_gb_merkle_trees = git https://github.com/KrzysiekJ/gb_merkle_trees.git v0.2.0
dep_jiffy = git https://github.com/davisp/jiffy.git 0.14.11
dep_libsodium = git https://github.com/potatosalad/erlang-libsodium.git 0.0.10
dep_nist_beacon = git https://gitlab.com/KrzysiekJ/nist_beacon v0.1.1
dep_triq = git https://github.com/triqng/triq.git babad

BUILD_DEPS = lfe lfe.mk
dep_lfe = git https://github.com/rvirding/lfe 2880c8a2
dep_lfe.mk = git https://github.com/KrzysiekJ/lfe.mk be7a892
DEP_PLUGINS = lfe.mk

# Whitespace to be used when creating files from templates.
SP = 4

include erlang.mk

# A workaround until https://github.com/ninenines/lfe.mk/issues/3 is fixed.
deps:: $(DEPS_DIR)/lfe/ebin/clj.beam
$(DEPS_DIR)/lfe/ebin/clj.beam:
	 $(verbose) PATH=$(PATH):$(DEPS_DIR)/lfe/bin lfec +debug_info -o $(DEPS_DIR)/lfe/ebin/ $(DEPS_DIR)/lfe/src/clj.lfe
