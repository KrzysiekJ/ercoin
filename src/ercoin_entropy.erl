-module(ercoin_entropy).

-export([reliable_entropy/1]).
-export([simple_entropy/1]).

-include_lib("include/ercoin.hrl").

-spec reliable_entropy(data()) -> binary().
%% @doc Obtain entropy for data in such way that should not be subject to manipulation.
reliable_entropy(#data{timestamp=Timestamp, epoch_length=EpochLength}) ->
    %% Drawing of validators waits half of the epoch before yielding result, so waiting for 1/100th of the epoch
    %% before obtaining entropy (to further ensure that it will not be manipulated) should leave us with plenty of time
    %% to wait in case of eventual beacon downtime.
    nist_beacon:pulse(Timestamp + EpochLength div 100).

-spec simple_entropy(data()) -> binary().
%% @doc Obtain pseudoentropy for data, possibly for development purposes.
%% Will be cheap and quick to generate but don’t expect it to be secure.
simple_entropy(#data{height=Height, timestamp=Timestamp}) ->
    ?HASH(<<Height, Timestamp:8/unit:8>>).
