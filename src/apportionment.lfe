;; For discussion of various apportionment schemes, see http://rangevoting.org/Apportion.html .

(defmodule sainte-lague
  "Apportionment using the Sainte-Laguë method."
  (export (apportion 2)
          (apportion 3)
          (apportion 4)))

(include-lib "include/apportionment.lfe")

(defspec (apportion 4)
  (((list (score)) (pos_integer) (UNION 'undefined (pos_integer)) (list (pos_integer))) (list (result))))
(defun apportion (scores seats _ divisors-init)
  (let* ((divisors-init-count (length divisors-init))
         (divisors (++ divisors-init (lists:seq (+ 1 (* 2 divisors-init-count)) (- (* 2 seats) 1) 2)))
         (quotients-ids (lc ((<- (tuple votes id) scores)
                             (<- divisor divisors))
                          (tuple (/ votes divisor) id)))
         (seats-singular (lists:sort
                          (lists:map
                           (match-lambda (((tuple _ id)) id))
                           (lists:sublist
                            (lists:sort (lambda (x y) (>= x y)) quotients-ids)
                            seats)))))
    (lists:foldr
     (match-lambda ((id (cons (tuple id seats) tail)) (cons (tuple id (+ 1 seats)) tail))
                   ((id tail) (cons (tuple id 1) tail)))
     (list)
     seats-singular)))

(defspec (apportion 3)
  (((list (score)) (pos_integer) (UNION 'undefined (pos_integer))) (list (result))))
(defun apportion (scores seats votes-sum)
  (apportion scores seats votes-sum '()))

(defspec (apportion 2)
  (((list (score)) (pos_integer)) (list (tuple (any) (pos_integer)))))
(defun apportion (scores seats)
  (apportion scores seats 'undefined))

(defmodule hare-niemeyer
  (export (apportion 2)
          (apportion 3)))

(include-lib "include/apportionment.lfe")

(defspec (add-singular-seats-and-flip 2)
  (((list (tuple (pos_integer) (any))) (list (any))) (list (result))))
(defun add-singular-seats-and-flip (results beneficients)
  (++
   (lists:map
    (match-lambda
      (((tuple seats id))
       (case (lists:member id beneficients)
         ('false (tuple id seats))
         ('true (tuple id (+ 1 seats))))))
    results)
   (lc ((<- id (-- beneficients
                   (lc ((<- (tuple _ id-existing) results))
                     id-existing))))
     (tuple id 1))))

(defspec (apportion 4)
  (((list (score)) (pos_integer) (pos_integer) (float)) (list (tuple (any) (pos_integer)))))
(defun apportion (scores total-seats _ quota)
  (let* (((tuple int-results part-results int-seats-sum)
          (lists:foldl
           (match-lambda
             (((tuple score id) (tuple int-results-acc part-results-acc int-seats-sum))
              (let* ((fraction-seats (/ score quota))
                     (int-seats (trunc fraction-seats))
                     (part-seats (- fraction-seats int-seats)))
                (tuple
                 (case int-seats
                   (0 int-results-acc)
                   (_ (cons (tuple int-seats id) int-results-acc)))
                 (cons (tuple part-seats id) part-results-acc)
                 (+ int-seats int-seats-sum)))))
           (tuple '() '() 0)
           scores))
         (part-seats-for
          (lists:map
           (match-lambda (((tuple _ id)) id))
           (lists:sublist
            (lists:sort (lambda (x y) (>= x y)) part-results)
            (- total-seats int-seats-sum)))))
    (lists:sort (add-singular-seats-and-flip int-results part-seats-for))))

(defspec (apportion 3)
  (((list (score)) (pos_integer) (pos_integer)) (list (tuple (any) (pos_integer)))))
(defun apportion (scores seats votes-sum)
  (apportion scores seats votes-sum (/ votes-sum seats)))

(defspec (apportion 2)
  (((list (score)) (pos_integer)) (list (tuple (any) (pos_integer)))))
(defun apportion (scores seats)
  (let ((total-votes (lists:foldl (match-lambda (((tuple votes _) acc) (+ votes acc))) 0 scores)))
    (apportion scores seats total-votes)))

(defmodule apportionment_test
  (export all))

(include-lib "lfe/include/clj.lfe")
(include-lib "triq/include/triq.hrl")
(include-lib "include/apportionment.lfe")

(defun score ()
  (tuple (non_neg_integer) (atom)))

(defspec (proplists_remove_value_duplicates 1)
  (((list (tuple (any) (any)))) (list (tuple (any) (any)))))
(defun proplists_remove_value_duplicates (proplist)
  (let (((tuple result _)
         (lists:foldr
          (match-lambda
            (((tuple key value) (tuple result-acc used))
             (case (sets:is_element value used)
               ('true (tuple result-acc used))
               ('false (tuple (cons (tuple key value) result-acc) (sets:add_element value used))))))
          (tuple (list) (sets:new))
          proplist)))
    result))

(defun scores ()
  (LET
   unordered-scores
   (SUCHTHAT
    scores
    (triq_dom:list (score))
    (lists:any (match-lambda (((tuple votes _)) (> votes 0))) scores))
   (proplists_remove_value_duplicates unordered-scores)))

(defspec (sum-scores 1)
  (((list (score))) (pos_integer)))
(defun sum-scores (scores)
  (lists:foldl (match-lambda (((tuple votes _) acc) (+ votes acc))) 0 scores))

(defun method ()
  (oneof '(hare-niemeyer sainte-lague)))

(defun prop_house-monotonicity ()
  "If population of whole country goes up (preserving proportions), no state can lose seats."
 (FORALL
  (tuple scores seats scale-factor method)
  (tuple (scores) (pos_integer) (pos_integer) 'sainte-lague)
  (let* ((scores-scaled (lists:map (match-lambda (((tuple votes id)) (tuple (* scale-factor votes) id))) scores))
         (apportionment (apply method 'apportion (list scores seats)))
         (apportionment-scaled (apply method 'apportion (list scores-scaled seats))))
    (=:= apportionment apportionment-scaled))))

(defun prop_population-pair-monotonicity ()
  "If population of state A increases but that of state B decreases, then A should not lose seats while B stays the same or gains seats."
  ;; http://rangevoting.org/Apportion.html generalizes this property as “if A’s percentage population change exceeds B’s, then A should not lose seats while B stays the same or gains seats.”, but that’s not true — consider cases where B has 0 seats initially.
  (FORALL
   (tuple scores-base (tuple (tuple vote-1 id-1) (tuple vote-2 id-2)) (tuple change-1 change-2) seats method)
   (SUCHTHAT
    (tuple scores-base (tuple (tuple vote-1 id-1) (tuple vote-2 id-2)) (tuple change-1 change-2) seats method)
    (tuple (scores) (tuple (score) (score)) (tuple (pos_integer) (pos_integer)) (pos_integer) 'sainte-lague)
    (andalso (=/= id-1 id-2)
             (> vote-2 0)
             (> vote-1 0)
             (not (lists:any (match-lambda (((tuple _ id)) (orelse (=:= id-1 id) (=:= id-2 id)))) scores-base))))
   (let* ((scores-initial (->> scores-base
                              (cons (tuple vote-1 id-1))
                              (cons (tuple vote-2 id-2))
                              (lists:sort)
                              (lists:reverse)))
          (scores-final (->> scores-base
                            (cons (tuple (+ vote-1 change-1) id-1))
                            (cons (tuple (- vote-2 change-2) id-2))
                            (lists:sort)
                            (lists:reverse)))
          (apportionment-initial (apply method 'apportion (list scores-initial seats)))
          (apportionment-final (apply method 'apportion (list scores-final seats))))
     (orelse (>= (proplists:get_value id-1 apportionment-final 0) (proplists:get_value id-1 apportionment-initial 0))
             (< (proplists:get_value id-2 apportionment-final 0) (proplists:get_value id-2 apportionment-initial 0))))))

(defun prop_quota ()
  "Every participant gets number of seats equal either to the floor or ceil of his share multiplied by total seats."
  (FORALL
   (tuple scores total-seats method)
   (tuple (scores) (pos_integer) 'hare-niemeyer)
   (let ((results (apply method 'apportion (list scores total-seats))))
     (lists:all
      (match-lambda
        (((tuple score id))
         (let ((seats (proplists:get_value id results 0))
               (expected (* (/ score (sum-scores scores)) total-seats)))
           (orelse (== seats (math:ceil expected)) (== seats (math:floor expected))))))
      scores))))

(defun prop_seats-sum ()
  "Apportioned seats sum to the desired number of total seats."
  (FORALL
   (tuple scores seats method)
   (tuple (scores) (pos_integer) (method))
   (=:= seats
        (lists:foldl
         (match-lambda (((tuple _ seats) acc) (+ seats acc)))
         0
         (apply method 'apportion (list scores seats))))))

(defun prop_uniqueness ()
  "Apportioned seats are unique by id."
  (FORALL
   (tuple scores seats method)
   (tuple (scores) (pos_integer) (method))
   (let ((results (apply method 'apportion (list scores seats))))
     (=:= (length results)
          (sets:size (sets:from_list (lc ((<- (tuple id _) results)) id)))))))

(defun prop_result_ordering ()
  "Apportioned seats are sorted."
  (FORALL
   (tuple scores seats method)
   (tuple (scores) (pos_integer) (method))
   (let ((results (apply method 'apportion (list scores seats))))
     (=:= results (lists:sort results)))))

(defun prop_score_ordering ()
  "In case of equal scores, problematic seats get assigned to one with higher id."
  (FORALL
   (tuple scores seats method)
   (tuple (scores) (pos_integer) (method))
   (let ((results (apply method 'apportion (list scores seats))))
     (lists:all
      (match-lambda
        (((tuple points id1))
         (let ((result1 (proplists:get_value id1 results 0)))
           (lists:all
            (lambda
                (id2)
                 (let ((result2 (proplists:get_value id2 results 0)))
                   (case (> id2 id1)
                     ('true (>= result2 result1))
                     ('false (=< result2 result1)))))
            (proplists:get_all_values points scores)))))
      scores))))
