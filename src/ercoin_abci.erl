%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_abci).
-behaviour(gen_statem).
-behaviour(abci_app).
-export(
   [start_link/0]).

-compile({parse_transform, dynarec}).

%% Some directives to ease Triq testing (see https://github.com/ninenines/erlang.mk/issues/699) and expose testing code to Dialyzer.
-ifndef(TEST).
-define(TEST, true).
-endif.
%% -compile(export_all).

%% gen_statem
-export(
   [callback_mode/0,
    code_change/4,
    gossiping/3,
    committing/3,
    init/1,
    terminate/3]).

%% abci_app
-export([handle_request/1]).

-export(
   [app_hash/1,
    new_validators/1]).

-include_lib("abci_server/include/abci.hrl").

-ifdef(TEST).
-export(
   [id/1]).

-include_lib("triq/include/triq.hrl").
-include_lib("eunit/include/eunit.hrl").
-endif.

-include_lib("include/ercoin.hrl").

start_link() ->
    gen_statem:start_link({local, ?MODULE}, ?MODULE, [], []).

callback_mode() ->
    state_functions.

code_change(_OldVsn, OldState, OldData, _Extra) ->
    {ok, OldState, OldData}.

init(_Args) ->
    {ok, gossiping, ercoin_persistence:current_data()}.

terminate(_Reason, _State, _Data) ->
    ok.

gossiping(internal, dump_data, Data) ->
    ok = ercoin_persistence:dump_data_async(Data),
    keep_state_and_data;
gossiping(
  {call, From},
  #'RequestBeginBlock'{
     absent_validators=AbsentValidatorsIndices,
     header=#'Header'{time=Timestamp}},
  Data=#data{validators=Validators}) ->
    ValidatorsList = gb_merkle_trees:to_orddict(Validators),
    NewValidators =
        lists:foldl(
          fun (I, Acc) ->
                  {PK, <<Power, Absencies:3/unit:8, VoteBin/binary>>} = lists:nth(I + 1, ValidatorsList),
                  NewAbsencies = Absencies + 1,
                  gb_merkle_trees:enter(PK, <<Power, NewAbsencies:3/unit:8, VoteBin/binary>>, Acc)
          end,
          Validators,
          AbsentValidatorsIndices),
    NewData =
        Data#data{
          validators=NewValidators,
          timestamp=Timestamp},
    {next_state, committing, NewData, {reply, From, #'ResponseBeginBlock'{}}};
gossiping({call, From}, #'RequestInitChain'{}, _Data) ->
    {keep_state_and_data, {reply, From, #'ResponseInitChain'{}}};
gossiping({call, From}, Request, Data) ->
    Response = gossiping(Request, Data),
    {keep_state_and_data, {reply, From, Response}}.

committing({call, From}, #'RequestCommit'{}, Data=#data{height=Height, epoch_length=EpochLength}) ->
    Response = #'ResponseCommit'{data=app_hash(Data)},
    Reply = {reply, From, Response},
    Actions =
        %% It is important that we dump data at such height that future_validators is not a promise.
        case Height rem EpochLength of
            0 ->
                [Reply, {next_event, internal, dump_data}];
            _ ->
                [Reply]
        end,
    {next_state, gossiping, Data, Actions};
committing(
  {call, From},
  #'RequestEndBlock'{},
  Data=
      #data{
         epoch_length=EpochLength,
         height=Height,
         fresh_txs=FreshTxs,
         validators=Validators,
         future_validators=FutureValidators}) ->
    Data1 =
        case Height rem EpochLength of
            0 ->
                grant_fee_deposits(Data);
            _ ->
                Data
        end,
    Stage = (Height + 1) rem EpochLength,
    {Data2, Diffs} =
        %% Drawing of new validators.
        if
            Stage =:= 0 ->
                {Data1#data{validators=FutureValidators, future_validators=undefined}, calculate_diffs(FutureValidators, Validators)};
            Stage =:= EpochLength div 4 ->
                Key = docile_rpc:async_call(node(), ?MODULE, new_validators, [Data]),
                {Data1#data{future_validators={promise, Key, app_hash(Data)}}, []};
            %% The gap between locking data for the purpose of drawing and yielding the result of drawing gives us some time in case
            %% entropy source is down.
            Stage =:= EpochLength * 3 div 4 ->
                {promise, Key, _} = FutureValidators,
                %% During normal operation, the result should be already available (some timeout may be needed anyway to deliver it),
                %% but when we’re syncing, we may need to wait a bit.
                {value, NewFutureValidators} = docile_rpc:nb_yield(Key, 7000),
                {Data1#data{future_validators=NewFutureValidators}, []};
            true ->
                {Data1, []}
        end,
    NewHeight = Data2#data.height + 1,
    NewFreshTxs = lists:dropwhile(fun ({ValidSince, _}) -> ValidSince < NewHeight - EpochLength + 2 end, FreshTxs),
    Data3 =
        remove_old_and_unlock_accounts(
          Data2#data{height=NewHeight, fresh_txs=NewFreshTxs}),
    {keep_state, Data3, {reply, From, #'ResponseEndBlock'{validator_updates=Diffs}}};
committing({call, From}, Request, Data) when is_record(Request, 'RequestDeliverTx')->
    {Response, NewData} = committing(Request, Data),
    {keep_state, NewData, {reply, From, Response}};
committing({call, _}, _, _) ->
    {keep_state_and_data, postpone}.

handle_request(Request) ->
    gen_statem:call(?MODULE, Request).

%% Internal functions.

-spec gossiping(abci_server:request(), data()) -> abci_server:response().
%% @doc A helper function used to return responses for gossiping requests.
gossiping(#'RequestQuery'{path=Path, data=QueryData}, #data{accounts=Accounts}) ->
    {Code, ResponseValue, Error} =
        case iolist_to_binary(Path) of
            <<"account">> ->
                case gb_merkle_trees:lookup(QueryData, Accounts) of
                    none ->
                        {?NOT_FOUND, <<>>, "Account not found."};
                    AccountBin ->
                        {?OK, AccountBin, []}
                end;
            _ ->
                {?BAD_REQUEST, <<>>, []}
        end,
    #'ResponseQuery'{code=Code, value=ResponseValue, log=Error};
gossiping(#'RequestCheckTx'{tx=TxBin}, Data) ->
    case tx_deserialize(TxBin) of
        none ->
            #'ResponseCheckTx'{code=?BAD_REQUEST};
        Tx ->
            #'ResponseCheckTx'{code=tx_error_code(Tx, Data)}
    end;
gossiping(#'RequestInfo'{}, Data) ->
    #'ResponseInfo'{
       last_block_app_hash=app_hash(Data),
       last_block_height=height(Data)}.

-spec committing(abci_server:request(), data()) -> {abci_server:response(), data()}.
committing(#'RequestDeliverTx'{tx=TxBin}, Data) ->
    case tx_deserialize(TxBin) of
        none ->
            {#'ResponseDeliverTx'{code=?BAD_REQUEST}, Data};
        Tx ->
            case tx_error_code(Tx, Data) of
                ?OK ->
                    Response = #'ResponseDeliverTx'{code=?OK},
                    NewData = apply_tx(Tx, Data),
                    {Response, NewData};
                ErrorCode ->
                    Response = #'ResponseDeliverTx'{code=ErrorCode},
                    {Response, Data}
            end
    end.

-spec calculate_diffs(gb_merkle_trees:tree(), gb_merkle_trees:tree()) -> list(#'Validator'{}).
calculate_diffs(New, Old) ->
    KeysFun =
        fun (Tree) ->
                gb_merkle_trees:foldr(
                  fun ({Key, _}, Acc) -> [Key|Acc] end,
                  [],
                  Tree)
        end,
    %% pub_key in Tendermint is prefixed with type byte (1 for Ed25519).
    DeletedEntries = [#'Validator'{pub_key= <<1, PK/binary>>, power=0} || PK <- KeysFun(Old) -- KeysFun(New)],
    NewEntries =
        [begin
             <<VP, _/binary>> = Value,
             #'Validator'{pub_key= <<1, PK/binary>>, power=VP}
         end || {PK, Value} <- gb_merkle_trees:to_orddict(New) -- gb_merkle_trees:to_orddict(Old)],
    NewEntries ++ DeletedEntries.

-spec get_account(pk(), data()) -> account() | none.
get_account(Address, #data{accounts=Accounts}) ->
    case gb_merkle_trees:lookup(Address, Accounts) of
        none ->
            none;
        AccountBin ->
            account_deserialize({Address, AccountBin})
    end.

-spec put_account(account(), data()) -> data().
put_account(Account, Data=#data{accounts=Accounts}) ->
    {Address, AccountBin} = account_serialize(Account),
    NewAccounts = gb_merkle_trees:enter(Address, AccountBin, Accounts),
    Data#data{accounts=NewAccounts}.

-spec is_locked(account()) -> boolean().
is_locked(#account{locked_until=LockedUntil}) ->
    LockedUntil =/= none.

-spec put_vote(pk(), vote(), data()) -> data().
put_vote(Address, Vote, Data=#data{validators=Validators, future_validators=FutureValidators, height=Height, epoch_length=EpochLength}) ->
    VoteBin = vote_serialize(Vote),
    NewValidators =
        case gb_merkle_trees:lookup(Address, Validators) of
            <<Power, AbsenciesBin:3/binary, _/binary>> ->
                gb_merkle_trees:enter(Address, <<Power, AbsenciesBin:3/binary, VoteBin/binary>>, Validators);
            none ->
                Validators
        end,
    NewFutureValidators =
        case Height rem EpochLength >= EpochLength * 3 div 4 of
            true ->
                case gb_merkle_trees:lookup(Address, FutureValidators) of
                    <<Power2, 0:3/unit:8, _/binary>> ->
                        gb_merkle_trees:enter(Address, <<Power2, 0:3/unit:8, VoteBin/binary>>, FutureValidators);
                    none ->
                        FutureValidators
                end;
            false ->
                FutureValidators
        end,
    Data#data{validators=NewValidators, future_validators=NewFutureValidators}.

-spec apply_tx(tx(), data()) -> data().
apply_tx(Tx, Data=#data{fee_deposit_short=FeeDepositShort, fee_deposit_long=FeeDepositLong}) ->
    From = get_account(from(Tx), Data),
    FeeShort = fee_short(Tx, Data),
    NewFeeDepositShort = FeeDepositShort + FeeShort,
    FeeLong = fee_long(Tx, Data),
    NewFeeDepositLong = FeeDepositLong + FeeLong,
    NewFrom = From#account{balance=From#account.balance - FeeShort - FeeLong},
    apply_tx_1(
      Tx,
      put_account(NewFrom, Data#data{fee_deposit_short=NewFeeDepositShort, fee_deposit_long=NewFeeDepositLong})).

-spec apply_tx_1(tx(), data()) -> data().
apply_tx_1(Tx=#transfer_tx{to=To, from=From, value=Value}, Data=#data{fresh_txs=FreshTxs, fresh_txs_hash=FreshTxsHash}) ->
    TxBin = tx_serialize(Tx),
    NewFreshTxs = ?SETS:add_element({get_valid_since(Tx), ?HASH(TxBin)}, FreshTxs),
    NewFreshTxsHash = ?HASH(<<FreshTxsHash/binary, TxBin/binary>>),
    FromAccount = get_account(From, Data),
    NewFromAccount = FromAccount#account{balance=FromAccount#account.balance - Value},
    Data1 = put_account(NewFromAccount, Data#data{fresh_txs=NewFreshTxs, fresh_txs_hash=NewFreshTxsHash}),
    ToAccount = get_account(To, Data1),
    NewToAccount = ToAccount#account{balance=ToAccount#account.balance + Value},
    put_account(NewToAccount, Data1);
apply_tx_1(#account_tx{valid_until=ValidUntil, to=ToAddress}, Data) ->
    NewAccount =
        case get_account(ToAddress, Data) of
            Account=#account{} ->
                Account#account{valid_until=ValidUntil};
            none ->
                #account{
                   valid_until=ValidUntil,
                   address=ToAddress}
        end,
    put_account(NewAccount, Data);
apply_tx_1(#lock_tx{locked_until=LockedUntil, address=Address}, Data) ->
    Account = get_account(Address, Data),
    put_account(Account#account{locked_until=LockedUntil}, Data);
apply_tx_1(Tx=#vote_tx{vote=Vote, address=Address}, Data=#data{fresh_txs=FreshTxs, fresh_txs_hash=FreshTxsHash}) ->
    TxBin = tx_serialize(Tx),
    NewFreshTxsHash = ?HASH(<<FreshTxsHash/binary, TxBin/binary>>),
    NewFreshTxs = ?SETS:add_element({get_valid_since(Tx), ?HASH(TxBin)}, FreshTxs),
    put_vote(Address, Vote, Data#data{fresh_txs=NewFreshTxs, fresh_txs_hash=NewFreshTxsHash}).

-spec tx_error_code(tx(), data()) -> error_code().
tx_error_code(Tx, Data=#data{height=Height, epoch_length=EpochLength}) ->
    ValidSince = get_valid_since(Tx),
    case ValidSince =:= none orelse ValidSince =< Height andalso ValidSince > Height - EpochLength of
        true ->
            tx_error_code_1(Tx, Data);
        _ ->
            ?INVALID_TIMESTAMP
    end.

-spec tx_error_code_1(tx(), data()) -> error_code().
tx_error_code_1(Tx=#transfer_tx{from=From, to=To, value=Value}, Data) ->
    case ?SETS:is_element({get_valid_since(Tx), ?HASH(tx_serialize(Tx))}, Data#data.fresh_txs) of
        false ->
            case get_account(To, Data) of
                none ->
                    ?NOT_FOUND;
                #account{} ->
                    case get_account(From, Data) of
                        none ->
                            ?NOT_FOUND;
                        #account{balance=Balance, locked_until=LockedUntil} ->
                            case LockedUntil of
                                none ->
                                    case Balance < fee(Tx, Data) + Value of
                                        false ->
                                            ?OK;
                                        true ->
                                            ?INSUFFICIENT_FUNDS
                                    end;
                                _ ->
                                    ?FORBIDDEN
                            end
                    end
            end;
        true ->
            ?ALREADY_EXECUTED
    end;
tx_error_code_1(#account_tx{valid_until=ValidUntil, to=To, from=From}, Data) ->
    case From =:= To orelse not is_locked(get_account(From, Data)) of
        true ->
            case get_account(To, Data) of
                none ->
                    ?OK;
                #account{valid_until=CurrentValidUntil} ->
                    case ValidUntil > CurrentValidUntil of
                        true ->
                            ?OK;
                        false ->
                            ?FORBIDDEN
                    end
            end;
        false ->
            ?FORBIDDEN
    end;
tx_error_code_1(#lock_tx{address=Address, locked_until=LockedUntil}, Data) ->
    #account{locked_until=CurrentLockedUntil, valid_until=ValidUntil} = get_account(Address, Data),
    case (CurrentLockedUntil =:= none orelse LockedUntil > CurrentLockedUntil) andalso LockedUntil =< ValidUntil of
        true ->
            ?OK;
        false ->
            ?FORBIDDEN
    end;
tx_error_code_1(Tx=#vote_tx{}, #data{fresh_txs=FreshTxs}) ->
    case ?SETS:is_element({get_valid_since(Tx), ?HASH(tx_serialize(Tx))}, FreshTxs) of
        false ->
            ?OK;
        _ ->
            ?ALREADY_EXECUTED
    end.

-spec account_serialize(account()) -> {Key :: binary(), Value :: binary()}.
account_serialize(
  #account{
     address=Address,
     balance=Balance,
     valid_until=ValidUntil,
     locked_until=LockedUntil}) ->
    {Address,
     case LockedUntil of
         none ->
             <<ValidUntil:4/unit:8, Balance:8/unit:8>>;
         _ ->
             <<ValidUntil:4/unit:8, Balance:8/unit:8, LockedUntil:4/unit:8>>
     end}.

-spec account_deserialize({Key :: binary(), Value :: binary()}) -> account().
account_deserialize({Address, AccountBin}) ->
    <<ValidUntil:4/unit:8, Balance:8/unit:8, Rest/binary>> = AccountBin,
    BaseAccount =
        #account{
           address=Address,
           balance=Balance,
           valid_until=ValidUntil},
    case Rest of
        <<>> ->
            BaseAccount;
        <<LockedUntil:4/unit:8>> ->
            BaseAccount#account{
              locked_until=LockedUntil}
    end.

-spec tx_deserialize(binary()) -> tx() | none.
tx_deserialize(TxBin) ->
    Tx =
        case TxBin of
            <<0, ValidSince:4/unit:8, From:32/binary, To:32/binary, Value:8/unit:8, MsgLength/integer, Msg:MsgLength/binary, Signature/binary>> ->
                #transfer_tx{
                   valid_since=ValidSince,
                   from=From,
                   to=To,
                   value=Value,
                   message=Msg,
                   signature=Signature};
            <<1, ValidUntil:4/unit:8, From:32/binary, To:32/binary, Signature/binary>> ->
                #account_tx{
                   valid_until=ValidUntil,
                   from=From,
                   to=To,
                   signature=Signature};
            <<2, LockedUntil:4/unit:8, Address:32/binary, Signature/binary>> ->
                #lock_tx{
                   locked_until=LockedUntil,
                   address=Address,
                   signature=Signature};
            <<3, ValidSince:4/unit:8, Address:32/binary, FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8, Protocol:1/unit:8, Signature/binary>> ->
                #vote_tx{
                   address=Address,
                   vote=
                       #vote{
                          valid_since=ValidSince,
                          choices=#{
                            fee_per_tx => FeePerTx,
                            fee_per_256_bytes => FeePer256B,
                            fee_per_account_day => FeePerAccountDay,
                            protocol => Protocol}},
                   signature=Signature};
            _ ->
                none
        end,
    case Tx of
        none ->
            none;
        _ ->
            Signature1 = get_value(signature, Tx),
            SignedMsg = binary:part(TxBin, 0, byte_size(TxBin) - byte_size(Signature1)),
            case ercoin_sig:verify_detached(Signature1, SignedMsg, from(Tx))  of
                true ->
                    Tx;
                false ->
                    none
            end
    end.

-spec height(data()) -> block_height() | 0.
height(#data{height=Height}) ->
    Height.

-spec app_hash(data()) -> binary().
app_hash(
  #data{
     protocol=Protocol,
     accounts=Accounts,
     validators=Validators,
     future_validators=FutureValidators,
     fresh_txs_hash=FreshTxsHash,
     fee_deposit_short=FeeDepositShort,
     fee_deposit_long=FeeDepositLong,
     height=Height,
     timestamp=Timestamp}) ->
    ValidatorsHash = gb_merkle_trees:root_hash(Validators),
    AccountsHash = gb_merkle_trees:root_hash(Accounts),
    FutureValidatorsHash =
        case FutureValidators of
            undefined ->
                <<0:256>>;
            {promise, _, Hash} ->
                Hash;
            _ ->
                gb_merkle_trees:root_hash(FutureValidators)
        end,
    OtherDataHash =
        ?HASH(
           <<Protocol,
             Height:4/unit:8,
             Timestamp:8/unit:8,
             FeeDepositShort:8/unit:8,
             FeeDepositLong:8/unit:8,
             ValidatorsHash/binary,
             FutureValidatorsHash/binary,
             FreshTxsHash/binary>>),
    ?HASH(<<AccountsHash/binary, OtherDataHash/binary>>).

-spec fees(data()) -> map().
fees(Data) ->
    maps:remove(
      protocol,
      maps:merge(
        #{fee_per_256_bytes => 0,
          fee_per_account_day => 1,
          fee_per_tx => 0},
        voted_choices(Data))).

-spec div_ceil(integer(), integer()) -> integer().
%% @doc Divide integers with rounding up.
div_ceil(Dividend, Divisor) ->
    Dividend div Divisor + min(Dividend rem Divisor, 1).

-spec fee(tx(), data()) -> fee().
fee(Tx, Data) ->
    fee_short(Tx, Data) + fee_long(Tx, Data).

-spec fee_long(tx(), data()) -> fee().
fee_long(#account_tx{valid_until=ValidUntil, to=Address}, Data=#data{height=Height}) ->
    ExtensionLength =
        case get_account(Address, Data) of
            none ->
                ValidUntil - Height;
            #account{valid_until=OldValidUntil} ->
                ValidUntil - OldValidUntil
        end,
    #{fee_per_account_day := FPerAccountDay} = fees(Data),
    div_ceil(ExtensionLength * FPerAccountDay, 3600 * 24);
fee_long(#lock_tx{locked_until=LockedUntil, address=Address}, Data=#data{height=Height}) ->
    #account{locked_until=OldLockedUntil} = get_account(Address, Data),
    ExtensionLength =
        case OldLockedUntil of
            none ->
                LockedUntil - Height;
            _ ->
                LockedUntil - OldLockedUntil
        end,
    #{fee_per_account_day := FPerAccountDay} = fees(Data),
    div_ceil(ExtensionLength * FPerAccountDay, 3600 * 24);
fee_long(_, _) ->
    0.

-spec fee_short(tx(), data()) -> fee().
fee_short(#vote_tx{}, _Data) ->
    0;
fee_short(Tx, Data) ->
    #{fee_per_256_bytes := FPer256B,
      fee_per_tx := FPerTx}
        = fees(Data),
    div_ceil(byte_size(tx_serialize(Tx)) * FPer256B, 256) + FPerTx.

-spec from(tx()) -> pk().
from(#transfer_tx{from=From}) ->
    From;
from(#account_tx{from=From}) ->
    From;
from(#lock_tx{address=Address}) ->
    Address;
from(#vote_tx{address=Address}) ->
    Address.

-spec tx_serialize(tx()) -> binary().
tx_serialize(
  #transfer_tx{
     valid_since=ValidSince,
     from=From,
     to=To,
     value=Value,
     message=Message,
     signature=Signature}) ->
    MessageLength = byte_size(Message),
    <<0, ValidSince:4/unit:8, From/binary, To/binary, Value:8/unit:8, MessageLength, Message/binary, Signature/binary>>;
tx_serialize(
  #account_tx{
     valid_until=ValidUntil,
     from=From,
     to=To,
     signature=Signature}) ->
    <<1, ValidUntil:4/unit:8, From/binary, To/binary, Signature/binary>>;
tx_serialize(
 #lock_tx{
    locked_until=LockedUntil,
    address=Address,
    signature=Signature}) ->
    <<2, LockedUntil:4/unit:8, Address/binary, Signature/binary>>;
tx_serialize(
 #vote_tx{
    vote=
        #vote{
           valid_since=ValidSince,
           choices=Choices},
    address=Address,
    signature=Signature}) ->
    ChoicesBin = choices_serialize(Choices),
    <<3, ValidSince:4/unit:8, Address/binary, ChoicesBin/binary, Signature/binary>>.

-spec choices_serialize(choices()) -> binary().
choices_serialize(
  #{fee_per_tx := FeePerTx,
    fee_per_256_bytes := FeePer256B,
    fee_per_account_day := FeePerAccountDay,
    protocol := Protocol}) ->
    <<FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8, Protocol>>.

-spec vote_serialize(vote() | none) -> binary().
vote_serialize(
  #vote{
     valid_since=ValidSince,
     choices=
         #{fee_per_tx := FeePerTx,
           fee_per_256_bytes := FeePer256B,
           fee_per_account_day := FeePerAccountDay,
           protocol := Protocol}}) ->
    <<ValidSince:4/unit:8, FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8, Protocol>>;
vote_serialize(none) ->
    <<>>.

-spec vote_deserialize(binary()) -> vote() | none.
vote_deserialize(<<ValidSince:4/unit:8, FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8, Protocol:1/unit:8>>) ->
    #vote{
       valid_since=ValidSince,
       choices=
           #{fee_per_tx => FeePerTx,
             fee_per_256_bytes => FeePer256B,
             fee_per_account_day => FeePerAccountDay,
             protocol => Protocol}};
vote_deserialize(<<>>) ->
    none.

-spec get_valid_since(tx()) -> block_height() | none.
get_valid_since(#transfer_tx{valid_since=ValidSince}) ->
    ValidSince;
get_valid_since(#vote_tx{vote=#vote{valid_since=ValidSince}}) ->
    ValidSince;
get_valid_since(_) ->
    none.

-spec voting_resolution(data()) -> pos_integer().
voting_resolution(_) ->
    64.

-spec entropy(data()) -> binary().
entropy(Data=#data{entropy_fun={M, F}}) ->
    apply(M, F, [Data]).

-spec new_validators(data()) -> gb_merkle_trees:tree().
new_validators(Data=#data{accounts=Accounts, height=Height, epoch_length=EpochLength}) ->
    Resolution = voting_resolution(Data),
    {PointsAddresses, TotalPoints} =
        gb_merkle_trees:foldr(
          fun (AccountSerialized, {PointsAddresses1, PointsSum}) ->
                  #account{address=Address, locked_until=LockedUntil, balance=Balance} = account_deserialize(AccountSerialized),
                  case LockedUntil =/= none andalso LockedUntil > Height of
                      false ->
                          {PointsAddresses1, PointsSum};
                      true ->
                          NextEpochStart = EpochLength * (Height div EpochLength + 1),
                          Handicap = max(0, (LockedUntil - NextEpochStart + 1) * Balance),
                          Entropy = entropy(Data),
                          RandomInt = binary:decode_unsigned(binary:part(?HASH(<<0, Entropy/binary, Address/binary>>), 0, 8)),
                          Points = RandomInt * Handicap,
                          {[{Points, Address}|PointsAddresses1], PointsSum + Points}
                  end
          end,
          {[], 0},
          Accounts),
    %% Hare-Niemeyer method is chosen over Sainte-Laguë due to better performance.
    gb_merkle_trees:from_orddict(
      [{Address, case gb_merkle_trees:lookup(Address, Data#data.validators) of
                     none ->
                         <<VotePower, 0:3/unit:8>>;
                     <<_, _:3/unit:8, VoteBin2/binary>> ->
                         <<VotePower, 0:3/unit:8, VoteBin2/binary>>
                 end} ||
          {Address, VotePower} <- 'hare-niemeyer':apportion(PointsAddresses, Resolution, TotalPoints)]).

-spec grant_fee_deposits(data()) -> data().
%% @doc Dispose deposits, assuming that the time for it has come.
grant_fee_deposits(
  Data=#data{
          validators=Validators,
          fee_deposit_long=DepositLong,
          fee_deposit_short=DepositShort,
          epoch_length=EpochLength}) ->
    GrantedDepositLong = DepositLong div ?FEE_DEPOSIT_LONG_RATIO,
    GrantedDeposit = DepositShort + GrantedDepositLong,
    case GrantedDeposit of
        0 ->
            Data;
        _ ->
            SharesValidators =
                [{VP, PK} || {PK, <<VP, Absencies:3/unit:8, _/binary>>} <- gb_merkle_trees:to_orddict(Validators), Absencies < EpochLength div 3],
            ValidatorsRewards = 'hare-niemeyer':apportion(SharesValidators, GrantedDeposit),
            Data1 =
                lists:foldl(
                  fun ({PK, Reward}, DataPrime) ->
                          #account{balance=Balance} = Account = get_account(PK, Data),
                          put_account(Account#account{balance=Balance + Reward}, DataPrime)
                  end,
                  Data,
                  ValidatorsRewards),
            Data1#data{fee_deposit_short=0, fee_deposit_long=DepositLong - GrantedDepositLong}
    end.

-spec remove_old_and_unlock_accounts(data()) -> data().
remove_old_and_unlock_accounts(Data=#data{accounts=AccountsTree, height=Height, fee_deposit_long=FeeDepositLong}) ->
    {NewAccountsTree, CollectedBalance} = remove_old_and_unlock_accounts_from_tree(AccountsTree, Height),
    Data#data{accounts=NewAccountsTree, fee_deposit_long=FeeDepositLong + CollectedBalance}.

-spec remove_old_and_unlock_accounts_from_tree(gb_merkle_trees:tree(), block_height()) -> {gb_merkle_trees:tree(), non_neg_integer()}.
remove_old_and_unlock_accounts_from_tree(AccountsTree, Height) ->
    gb_merkle_trees:foldr(
      fun ({Address, AccountBin}, {TreeAcc, BalanceAcc}) ->
              case account_deserialize({Address, AccountBin}) of
                  #account{valid_until=ValidUntil, balance=Balance} when ValidUntil < Height ->
                      {gb_merkle_trees:delete(Address, TreeAcc), BalanceAcc + Balance};
                  Account=#account{locked_until=LockedUntil} when LockedUntil < Height ->
                      {_, NewAccountBin} = account_serialize(Account#account{locked_until=none}),
                      {gb_merkle_trees:enter(Address, NewAccountBin, TreeAcc), BalanceAcc};
                  #account{} ->
                      {TreeAcc, BalanceAcc}
              end
      end,
      {AccountsTree, 0},
      AccountsTree).

-spec merge_choices_vps(choices(), voting_power(), map()) -> map().
merge_choices_vps(SingleChoices, VP, ChoicesVPs) ->
    Keys = maps:keys(ChoicesVPs),
    lists:foldl(
      fun (Key, Acc) ->
              ChoiceVPsForKey = maps:get(Key, Acc),
              Acc#{Key => [{maps:get(Key, SingleChoices), VP}|ChoiceVPsForKey]}
      end,
      ChoicesVPs,
      Keys).

-spec voted_choices(data()) -> choices().
voted_choices(#data{validators=Validators}) ->
    {VoteSum, ChoicesVPs} =
        gb_merkle_trees:foldr(
          fun ({_, <<VP/integer, _:3/binary, VoteBin/binary>>}, {VPSum, ChoicesVPs}) ->
                  case vote_deserialize(VoteBin) of
                      none ->
                          {VPSum, ChoicesVPs};
                      #vote{choices=Choices} ->
                          {VPSum + VP, merge_choices_vps(Choices, VP, ChoicesVPs)}
                  end
          end,
          {0, #{}},
          Validators),
    SortedChoicesVPs = maps:map(fun (_, ChoiceVPs) -> lists:sort(ChoiceVPs) end, ChoicesVPs),
    maps:map(
      fun (_, ChoiceVPs) -> quantiles:'weighted-quantile'(0.5, ChoiceVPs, VoteSum) end,
      SortedChoicesVPs).

%% TODO: Try to put tests into a separate module.
-ifdef(TEST).
-type secret() :: {SK :: <<_:512>>, {PK :: <<_:256>>, Path :: list({left | right, <<_:256>>}) | none}}.

-spec id(any()) -> any().
%% @doc Identity.
id(Whatever) ->
    Whatever.

-spec delete_account(pk(), data()) -> data().
delete_account(Address, Data=#data{accounts=Accounts}) ->
    NewAccounts = gb_merkle_trees:delete(Address, Accounts),
    Data#data{accounts=NewAccounts}.

lookup_balance(Address, #data{accounts=AccountsTree}) ->
    #account{balance=Balance} = account_deserialize({Address, gb_merkle_trees:lookup(Address, AccountsTree)}),
    Balance.

set_valid_since(ValidSince, Tx, SK) ->
    sign_tx(set_valid_since_1(ValidSince, Tx), SK).

set_valid_since_1(ValidSince, Tx=#transfer_tx{}) ->
    Tx#transfer_tx{valid_since=ValidSince};
set_valid_since_1(ValidSince, Tx=#vote_tx{}) ->
    Tx#vote_tx{vote=Tx#vote_tx.vote#vote{valid_since=ValidSince}}.

-spec sign_tx(tx(), secret() | map()) -> tx().
sign_tx(Tx, Secrets) when is_map(Secrets) ->
    Secret = maps:get(from(Tx), Secrets),
    sign_tx(Tx, Secret);
sign_tx(Tx, Secret) ->
    ToSign = to_sign(Tx),
    Signature = sign_detached(ToSign, Secret),
    set_value(signature, Signature, Tx).

-spec sign_detached(binary(), secret()) -> binary().
sign_detached(Msg, {SK, Proof}) ->
    RawSig = libsodium_crypto_sign_ed25519:detached(Msg, SK),
    ProofBin =
        case Proof of
            none ->
                <<>>;
            _ ->
                merkle_proofs:serialize(Proof)
        end,
    <<RawSig/binary, ProofBin/binary>>.

-spec sign(binary(), secret()) -> binary().
sign(Msg, SK) ->
    Signature = sign_detached(Msg, SK),
    <<Msg/binary, Signature/binary>>.

-spec get_vote(pk(), data()) -> vote() | none.
get_vote(Address, Data) ->
    get_vote(Address, Data, validators).

-spec get_vote(pk(), data(), atom()) -> vote() | none.
get_vote(Address, Data, Field) ->
    <<_:1/unit:8, _:3/unit:8, VoteBin/binary>> = gb_merkle_trees:lookup(Address, get_value(Field, Data)),
    vote_deserialize(VoteBin).

-spec to_sign(tx()) -> binary().
to_sign(
  #transfer_tx{
        valid_since=ValidSince,
        from=From,
        to=To,
        value=Value,
        message=Message}) ->
    MessageLength = byte_size(Message),
    <<0, ValidSince:4/unit:8, From/binary, To/binary, Value:8/unit:8, MessageLength, Message/binary>>;
to_sign(#account_tx{valid_until=ValidUntil, from=From, to=To}) ->
    <<1, ValidUntil:4/unit:8, From/binary, To/binary>>;
to_sign(#lock_tx{address=Address, locked_until=LockedUntil}) ->
    <<2, LockedUntil:4/unit:8, Address/binary>>;
to_sign(
  #vote_tx{
     address=Address,
     vote=
         #vote{
            valid_since=ValidSince,
            choices=Choices}}) ->
    ChoicesBin = choices_serialize(Choices),
    <<3, ValidSince:4/unit:8, Address/binary, ChoicesBin/binary>>.

money_supply(#data{accounts=Accounts, fee_deposit_short=FeeDepositShort, fee_deposit_long=FeeDepositLong}) ->
    AccountsBalance =
        gb_merkle_trees:foldr(
          fun (AccountSerialized, Sum) ->
                  #account{balance=Balance} = account_deserialize(AccountSerialized),
                  Balance + Sum
          end,
          0,
          Accounts),
    AccountsBalance + FeeDepositShort + FeeDepositLong.

-spec uniqify_list(list(any())) -> list(any()).
uniqify_list(L) ->
    sets:to_list(sets:from_list(L)).

-spec apply_diffs(list(#'Validator'{}), gb_merkle_trees:tree()) -> gb_merkle_trees:tree().
apply_diffs([], Validators) ->
    Validators;
apply_diffs([#'Validator'{power=VP, pub_key= <<1, PubKey/binary>>}|Diffs], Validators) ->
    NewValidators =
        case VP of
            0 ->
                gb_merkle_trees:delete(PubKey, Validators);
            _ ->
                RestBin =
                    case gb_merkle_trees:lookup(PubKey, Validators) of
                        <<_:1/unit:8, RestBin2/binary>> ->
                            RestBin2;
                        none ->
                            <<0:3/unit:8>>
                    end,
                gb_merkle_trees:enter(PubKey, <<VP, RestBin/binary>>, Validators)
        end,
    apply_diffs(Diffs, NewValidators).

-spec end_block(data()) -> data().
end_block(Data) ->
    {keep_state, NewData, {reply, "from", #'ResponseEndBlock'{}}} =
        committing({call, "from"}, #'RequestEndBlock'{}, Data),
    NewData.

-define(
   SAMPLE_TRANSFER_TX,
   #transfer_tx{
      valid_since=1234,
      %% Secret key: <<241,233,155,25,231,68,253,237,185,176,117,196,7,138,248,241,14,148,35,27,241,3,132,194,142,70,50,81,135,104,72,21,236,168,42,39,132,221,157,252,126,127,245,49,190,10,84,207,79,253,85,117,4,82,163,98,146,75,91,17,19,215,36,37>>.
      from= <<236,168,42,39,132,221,157,252,126,127,245,49,190,10,84,207,79,253,85,117,4,82,163,98,146,75,91,17,19,215,36,37>>,
      to= <<0:32/unit:8>>,
      value=123456,
      message= <<"Zażółć gęślą jaźń."/utf8>>,
      signature= <<175,71,238,101,235,169,214,87,200,229,114,139,31,18,224,241,86,128,170,117,162,14,132,221,46,227,217,31,3,137,28,61,126,167,204,237,192,14,61,12,32,149,101,240,124,231,12,109,165,239,19,37,232,202,119,34,86,216,93,69,18,253,68,5>>}).
-define(
   SAMPLE_TRANSFER_TX_SERIALIZED,
   <<0,
     0,0,4,210,
     236,168,42,39,132,221,157,252,126,127,245,49,190,10,84,207,79,253,85,117,4,82,163,98,146,75,91,17,19,215,36,37,
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
     0,0,0,0,0,1,226,64,
     27,
     90,97,197,188,195,179,197,130,196,135,32,103,196,153,197,155,108,196,133,32,106,97,197,186,197,132,46,
     175,71,238,101,235,169,214,87,200,229,114,139,31,18,224,241,86,128,170,117,162,14,132,221,46,227,217,31,3,137,28,61,126,167,204,237,192,14,61,12,32,149,101,240,124,231,12,109,165,239,19,37,232,202,119,34,86,216,93,69,18,253,68,5>>).

tx_serialization_test_() ->
    [?_assertEqual(?SAMPLE_TRANSFER_TX_SERIALIZED, tx_serialize(?SAMPLE_TRANSFER_TX)),
     ?_assertEqual(?SAMPLE_TRANSFER_TX, tx_deserialize(?SAMPLE_TRANSFER_TX_SERIALIZED))].

%% Triq generators.

pk_sk() ->
    domain(
      pk_sk,
      fun (Self, _) -> {Self, libsodium_crypto_sign_ed25519:keypair()} end,
      fun (Self, Value) -> {Self, Value} end).

address_secret() ->
    oneof(
      [?LET(
          {PK, SK},
          pk_sk(),
          {PK, {SK, none}}),
       ?LET(
          {{PK, SK}, Path},
          {pk_sk(), merkle_proofs_test:'merkle-path'()},
          {merkle_proofs:fold({PK, Path}), {SK, {PK, Path}}})]).

partition(Int) ->
    domain(
      partition,
      fun (Self, _) -> {Self, partition_1(Int, [])} end,
      fun (Self, Partition) ->
              case Partition of
                  [_] ->
                      {Self, Partition};
                  _ ->
                      Len = length(Partition),
                      Idx1 = triq_rnd:uniform(Len),
                      Idx2 = triq_rnd:uniform(Len - 1),
                      Val1 = lists:nth(Idx1, Partition),
                      Val2 = lists:nth(Idx2, lists:delete(Val1, Partition)),
                      JoinedVal = Val1 + Val2,
                      NewPartition = [JoinedVal|lists:delete(Val2, lists:delete(Val1, Partition))],
                      {Self, NewPartition}
              end
      end).

partition_1(0, Acc) ->
    Acc;
partition_1(Int, Acc) ->
    Val = triq_rnd:uniform(Int),
    partition_1(Int - Val, [Val|Acc]).

pos_int_lte_with_partition(UpperBound) ->
    ?LET(
       I,
       choose(1, UpperBound),
       {I, partition(I)}).

partition_lte(UpperBound) ->
    ?LET(
       {_, Partition},
       pos_int_lte_with_partition(UpperBound),
       Partition).

hash() ->
    binary(32).

block_height() ->
    choose(1, ?MAX_BLOCK_HEIGHT div 2).

epoch_length() ->
    ?LET(
       I,
       non_neg_integer(),
       300 + I).

block_height(EpochLength) ->
    ?LET(
       {I, J},
       {non_neg_integer(), oneof([return(0), return(EpochLength div 4), return(EpochLength * 3 div 4), pos_integer()])},
       EpochLength * I + J - 1).

epoch_length_block_height() ->
    ?LET(
       EpochLength,
       epoch_length(),
       {EpochLength, block_height(EpochLength)}).

timestamp() ->
    pos_integer().

fee() ->
    frequency([{1, return(0)}, {3, pos_integer()}]).

balance() ->
    non_neg_integer().

message() ->
    ?SUCHTHAT(
       Bin,
       binary(),
       byte_size(Bin) < 256).

vote(#data{height=Height, epoch_length=EpochLength}) ->
    vote(Height, EpochLength).

vote(Height, EpochLength) ->
    ?LET(
       {ValidSince, Choices},
       {valid_since(Height, EpochLength), choices()},
       #vote{
          valid_since=ValidSince,
          choices=Choices}).

vote_tx_secret() ->
    ?LET(
       {{Address, Secret}, ValidSince, Choices},
       {address_secret(), block_height(), choices()},
       {sign_tx(
          #vote_tx{
             address=Address,
             vote=
                 #vote{
                    valid_since=ValidSince,
                    choices=Choices},
             signature= <<0:512>>},
          Secret),
        Secret}).

vote_tx() ->
    ?LET(
       {Tx, _},
       vote_tx_secret(),
       Tx).

vote_tx(Data, Address, SK) ->
    ?LET(
       Vote,
       vote(Data),
       sign_tx(
         #vote_tx{
            address=Address,
            vote=Vote,
            signature= <<0:512>>},
         SK)).

choices() ->
    ?LET(
       [FeePerTx, FeePer256B, FeePerAccountDay],
       vector(3, fee()),
       #{fee_per_tx => FeePerTx,
         fee_per_256_bytes => FeePer256B,
         fee_per_account_day => FeePerAccountDay,
         protocol => 1}).

account_secret(Locked, BlockHeight) ->
    LockedForGen =
        case Locked of
            none ->
                frequency([{1, return(0)}, {3, pos_integer()}, {3, return(none)}]);
            true ->
                frequency([{1, return(0)}, {3, pos_integer()}])
        end,
    ?LET(
       {{Address, Secret}, Balance, MaybeValidFor, MaybeLockedFor},
       {address_secret(), balance(), frequency([{1, return(0)}, {3, pos_integer()}]), LockedForGen},
       begin
           {ValidUntil, LockedUntil} =
               case MaybeLockedFor of
                   none ->
                       {BlockHeight + MaybeValidFor, none};
                   _ ->
                       case MaybeLockedFor > MaybeValidFor of
                           %% Account cannot be locked until longer than it is valid, so we swap the numbers.
                           true ->
                               {BlockHeight + MaybeLockedFor, BlockHeight + MaybeValidFor};
                           false ->
                               {BlockHeight + MaybeValidFor, BlockHeight + MaybeLockedFor}
                       end
               end,
           {#account{
               address=Address,
               balance=Balance,
               valid_until=ValidUntil,
               locked_until=LockedUntil},
            Secret}
       end).

account_secret(Locked) ->
    account_secret(Locked, 1).

account_secret() ->
    account_secret(none).

account() ->
    ?LET(
       {Account, _},
       account_secret(),
       Account).

-spec accounts_tree_from_list(list(account())) -> gb_merkle_trees:tree().
accounts_tree_from_list(Accounts) ->
    accounts_tree_from_list(Accounts, gb_merkle_trees:empty()).

accounts_tree_from_list([], Acc) ->
    Acc;
accounts_tree_from_list([Account|Tail], Acc) ->
    {Key, Value} = account_serialize(Account),
    NewAcc = gb_merkle_trees:enter(Key, Value, Acc),
    accounts_tree_from_list(Tail, NewAcc).

validators_tree_from_list([], Acc) ->
    Acc;
validators_tree_from_list([{{#account{address=Address}, Vote, Absencies}, VotePower}|Tail], Acc) ->
    VoteSerialized = vote_serialize(Vote),
    VotePowerBin = binary:encode_unsigned(VotePower),
    NewAcc = gb_merkle_trees:enter(Address, <<VotePowerBin/binary, Absencies:3/unit:8, VoteSerialized/binary>>, Acc),
    validators_tree_from_list(Tail, NewAcc).

-spec validators_tree_from_list(list({{account(), vote() | none}, pos_integer()})) -> gb_merkle_trees:tree().
validators_tree_from_list(AccountsVotesVotePowers) ->
    validators_tree_from_list(AccountsVotesVotePowers, gb_merkle_trees:empty()).

future_validators_accounts_sks(Height, EpochLength) ->
    %% Currently there is no possibility for this generator to return accounts that are also
    %% current validators.
    NextEpochStart = EpochLength * (Height div EpochLength + 1),
    VotingStart = EpochLength * (Height div EpochLength) + EpochLength * 3 div 4,
    ?LET(
       {Partition, PromiseHash},
       {partition_lte(255), hash()},
       ?LET(
          ValidatorsSKsVotes,
          vector(
            length(Partition),
            {account_secret(true, NextEpochStart),
             %% If there has been no drawing yet, there can be no votes (at least for validators that are not present in the current set).
             %% We ignore this problem for now.
             oneof([vote(VotingStart, EpochLength), none])}),
          begin
              Stage = Height rem EpochLength,
              ValidatorsSKs = [{Validator, SK} || {{Validator, SK}, _} <- ValidatorsSKsVotes],
              ValidatorsVotesAbsenciesVPs = [{{Validator, Vote, 0}, VP} || {{{Validator, _}, Vote}, VP} <- lists:zip(ValidatorsSKsVotes, Partition)],
              ValidatorsTree = validators_tree_from_list(ValidatorsVotesAbsenciesVPs),
              ReturnedValidatorsTree =
                  if
                      Stage < EpochLength div 4 ->
                          undefined;
                      Stage < EpochLength * 3 div 4 ->
                          Key = docile_rpc:async_call(node(), ?MODULE, id, [ValidatorsTree]),
                          {promise, Key, PromiseHash};
                      true ->
                          ValidatorsTree
                  end,
              {ReturnedValidatorsTree, ValidatorsSKs}
          end)).

data_sks(MustHaveAnUnlockedAccount) ->
    %% We use noshrink because shrinking is slow.
    noshrink(data_sks_1(MustHaveAnUnlockedAccount)).

data_sks_1(MustHaveAnUnlockedAccount) ->
    ?LET(
       {{EpochLength, Height},
        Timestamp,
        FreshTxsHash,
        FeeDepositShort,
        FeeDepositLong,
        VotesPartition,
        FreshTxsStub},
       {epoch_length_block_height(),
        timestamp(),
        hash(),
        fee(),
        fee(),
        partition_lte(255),
        list({block_height(), hash()})},
       ?LET(
          {ValidatorsSKsVotesAbsencies,
           {FutureValidatorsTree, FutureValidatorsSKs},
           AccountsSKs},
          {?SUCHTHAT(
              ValidatorsSKsVotesAbsencies,
              vector(
                length(VotesPartition),
                {account_secret(true, Height),
                 oneof([vote(Height, EpochLength), none]),
                 choose(0, (Height rem EpochLength) + 1)}),
              lists:any(
                fun (
                  {{#account{locked_until=LockedUntil, balance=Balance}, _}, _, _})
                    -> LockedUntil > Height andalso Balance > 0
                end,
                ValidatorsSKsVotesAbsencies) andalso
              lists:any(
                  fun ({_, _, Absencies}) -> Absencies =< (Height rem EpochLength) div 3 end,
                ValidatorsSKsVotesAbsencies)
               ),
           future_validators_accounts_sks(Height, EpochLength),
           case MustHaveAnUnlockedAccount of
               true ->
                   ?SUCHTHAT(
                      AccountsSKs,
                      list(account_secret(none, Height)),
                      lists:any(
                        fun ({Account, _}) -> not is_locked(Account) end,
                        AccountsSKs));
               false ->
                   list(account_secret(none, Height))
           end},
          begin
              FreshTxs =
                  [{max(1, Height - (TxHeight rem EpochLength)), Hash} || {TxHeight, Hash} <- FreshTxsStub],
              Validators = [Validator || {{Validator, _}, _, _} <- ValidatorsSKsVotesAbsencies],
              ValidatorsVotesAbsencies = [{Validator, Vote, Absencies} || {{Validator, _}, Vote, Absencies} <- ValidatorsSKsVotesAbsencies],
              ValidatorsVotesAbsenciesVPs = lists:zip(ValidatorsVotesAbsencies, VotesPartition),
              ValidatorsSKs = [{Validator, SK} || {{Validator, SK}, _, _} <- ValidatorsSKsVotesAbsencies],
              Accounts =
                  [Account || {Account, _} <- AccountsSKs],
              FutureValidators = [Account || {Account, _} <- FutureValidatorsSKs],
              SKs =
                  maps:from_list(
                    [{Address, SK} || {#account{address=Address}, SK} <- ValidatorsSKs ++ FutureValidatorsSKs ++ AccountsSKs]),
              AllAccounts = Accounts ++ Validators ++ FutureValidators,
              {#data{
                  height=Height,
                  timestamp=Timestamp,
                  epoch_length=EpochLength,
                  fee_deposit_short=FeeDepositShort,
                  fee_deposit_long=FeeDepositLong,
                  fresh_txs_hash=FreshTxsHash,
                  fresh_txs=?SETS:from_list(FreshTxs),
                  validators=validators_tree_from_list(ValidatorsVotesAbsenciesVPs),
                  future_validators=FutureValidatorsTree,
                  accounts=accounts_tree_from_list(AllAccounts),
                  entropy_fun={ercoin_entropy, simple_entropy}},
               SKs}
          end)).

data_sks() ->
    data_sks(false).

data() ->
    noshrink(
      ?LET(
         {Data, _},
         data_sks(),
         Data)).

header(#data{timestamp=Timestamp}) ->
    ?LET(
       {TimestampOffset},
       {pos_integer()},
       #'Header'{
          time=Timestamp + TimestampOffset}).

-spec filter_accounts(fun((account(), data()) -> boolean()), data()) -> list(account()).
filter_accounts(Pred, Data=#data{accounts=AccountsTree}) ->
    gb_merkle_trees:foldr(
      fun (AccountSerialized, Acc) ->
              Account = account_deserialize(AccountSerialized),
              case Pred(Account, Data) of
                  true ->
                      [Account|Acc];
                  false ->
                      Acc
              end
      end,
      [],
      AccountsTree).

account_from(Data=#data{height=Height, epoch_length=EpochLength}, Type) ->
    AccountFilter =
       case Type of
           locked ->
               fun (#account{locked_until=LockedUntil}, _) -> LockedUntil =/= none end;
           validator_current_or_future ->
               fun (#account{address=Address}, Data1) ->
                       gb_merkle_trees:lookup(Address, Data1#data.validators) =/= none orelse
                           Height rem EpochLength >= EpochLength * 3 div 4 andalso gb_merkle_trees:lookup(Address, Data1#data.future_validators) =/= none
               end;
           any ->
               fun (_, _) -> true end;
           unlocked ->
               fun (#account{locked_until=LockedUntil}, _) -> LockedUntil =:= none end
       end,
    AccountsList = filter_accounts(AccountFilter, Data),
    elements(AccountsList).

account_from(Data) ->
    account_from(Data, any).

data_sks_and_account(Type) ->
    ?LET(
       {Data, SKs},
       data_sks(),
       ?LET(
          Account,
          account_from(Data, Type),
          {{Data, SKs}, Account})).

data_sks_and_account() ->
    data_sks_and_account(any).

data_with_account_secret_and_vote_tx() ->
    ?LET(
       {{Data, SKs}, Account},
       data_sks_and_account(validator_current_or_future),
       begin
           SK = maps:get(Account#account.address, SKs),
           {Data, {Account, SK}, vote_tx(Data, Account#account.address, SK)}
       end).

data_with_vote_tx() ->
    ?LET(
       {Data, _, VoteTx},
       data_with_account_secret_and_vote_tx(),
       {Data, VoteTx}).

data_with_account() ->
    ?LET(
       {{Data, _}, Account},
       data_sks_and_account(),
       {Data, Account}).

data_sks_and_lock_tx() ->
    ?LET(
       {{{Data=#data{height=Height}, SKs}, Account=#account{address=Address, locked_until=LockedUntil, valid_until=ValidUntil}}, LockedFor},
       {data_sks_and_account(), pos_integer()},
       begin
           NewLockedUntil =
               case LockedUntil of
                   none ->
                       Height + LockedFor;
                   _ ->
                       LockedUntil + LockedFor
               end,
           NewAccount=Account#account{valid_until=max(NewLockedUntil, ValidUntil)},
           Tx =
               sign_tx(
                 #lock_tx{
                    address=Address,
                    locked_until=NewLockedUntil,
                    signature= <<0:512>>},
                 SKs),
           {{make_sufficient_balance(put_account(NewAccount, Data), Tx), SKs}, Tx}
       end).

valid_since(#data{height=Height, epoch_length=EpochLength}) ->
    valid_since(Height, EpochLength).

valid_since(Height, EpochLength) ->
    choose(max(Height - EpochLength + 1, 1), Height).


-spec make_sufficient_balance(data(), tx()) -> data().
make_sufficient_balance(Data, Tx) ->
    Account = get_account(from(Tx), Data),
    Fee = fee(Tx, Data),
    MinBalance =
        case Tx of
            #transfer_tx{value=Value} ->
                Fee + Value;
            _ ->
                Fee
        end,
    NewAccount = Account#account{balance=max(MinBalance, Account#account.balance)},
    put_account(NewAccount, Data).

transfer_tx({Data, SKs}, From, To) ->
    ?LET(
       {Value, ValidSince, Message},
       {choose(0, From#account.balance), valid_since(Data), message()},
       sign_tx(
         #transfer_tx{
            valid_since=ValidSince,
            from=From#account.address,
            to=To#account.address,
            message=Message,
            value=Value,
            signature= <<0:512>>},
         SKs
        )).

account_tx({Data, SKs}, From, ToAddress) ->
    ?LET(
       ValidFor,
       pos_integer(),
       begin
           ValidUntil =
               case get_account(ToAddress, Data) of
                   none ->
                       Data#data.height + ValidFor;
                   #account{valid_until=ExistingValidUntil} ->
                       ExistingValidUntil + ValidFor
               end,
           sign_tx(
             #account_tx{
                from=From#account.address,
                to=ToAddress,
                valid_until=ValidUntil,
                signature= <<0:512>>},
             SKs)
       end).

data_sks_and_transfer_tx() ->
    ?LET(
       {Data, SKs},
       data_sks(true),
       ?LET(
          {From, To},
          {account_from(Data, unlocked), account_from(Data)},
          ?LET(
             Tx,
             transfer_tx({Data, SKs}, From, To),
             {{make_sufficient_balance(Data, Tx), SKs}, Tx}))).

data_sks_and_account_tx() ->
    ?LET(
       {{Data, SKs}, From=#account{address=FromAddress}},
       data_sks_and_account(),
       ?LET(
          {ToAddress, ToSK},
          case is_locked(From) of
              true ->
                  {FromAddress, maps:get(FromAddress, SKs)};
              false ->
                  oneof(
                    [?LET(
                        #account{address=ToAddress},
                        account_from(Data),
                        {ToAddress, maps:get(ToAddress, SKs)}),
                     address_secret()])
          end,
          ?LET(
             Tx,
             account_tx({Data, SKs}, From, ToAddress),
             {{make_sufficient_balance(Data, Tx), maps:put(ToAddress, ToSK, SKs)}, Tx}))).

data_with_account_tx() ->
    ?LET(
       {{Data, _}, Tx},
       data_sks_and_account_tx(),
       {Data, Tx}).

data_with_transfer_tx() ->
    ?LET(
       {{Data, _}, Tx},
       data_sks_and_transfer_tx(),
       {Data, Tx}).

data_with_account_secret_and_transfer_tx() ->
    ?LET(
       {{Data, SKs}, Tx},
       data_sks_and_transfer_tx(),
       {Data, {get_account(Tx#transfer_tx.from, Data), maps:get(Tx#transfer_tx.from, SKs)}, Tx}).

data_with_lock_tx() ->
    ?LET(
       {{Data, _}, Tx},
       data_sks_and_lock_tx(),
       {Data, Tx}).

data_with_valid_tx() ->
    oneof(
      [fun data_with_transfer_tx/0,
       fun data_with_lock_tx/0]).

transfer_tx() ->
    ?LET(
       {_, ValidTx},
       data_with_transfer_tx(),
       ValidTx).

valid_lock_tx() ->
    ?LET(
       {_, Tx},
       data_with_lock_tx(),
       Tx).

account_tx() ->
    ?LET(
       {{_, _}, Tx},
       data_sks_and_account_tx(),
       Tx).

data_with_invalid_tx_bin() ->
    %% Tx is returned as binary.
    oneof(
      [%% Random binary.
       ?LET(
          {Data, RandomBin},
          {data(), binary()},
          {Data, RandomBin}),
       %% Valid tx with random bytes appended/prepended.
       ?LET(
          {{Data, Tx}, Prefix, Suffix},
          ?SUCHTHAT(
             {_, Prefix, Suffix},
             {data_with_valid_tx(), binary(), binary()},
             Prefix =/= <<>> orelse Suffix =/= <<>>),
          begin
              TxBin = tx_serialize(Tx),
              {Data, <<Prefix/binary, TxBin/binary, Suffix/binary>>}
          end),
       %% Insufficient balance for transfer.
       ?LET(
          {{{Data, SKs}, Tx}, LackingFunds},
          {data_sks_and_transfer_tx(), pos_integer()},
          begin
              Account = get_account(from(Tx), Data),
              InvalidValue = max(0, Account#account.balance - fee(Tx, Data) + LackingFunds),
              InvalidTx =
                  sign_tx(Tx#transfer_tx{value=InvalidValue}, SKs),
              {Data, tx_serialize(InvalidTx)}
          end),
       %% Bad signature.
       ?LET(
          {{Data, ValidTx}, InvalidSig},
          {data_with_valid_tx(), oneof([binary(64), binary()])},
          begin
              ToSign = to_sign(ValidTx),
              {Data, <<ToSign/binary, InvalidSig/binary>>}
          end),
       %% Lock not longer than existing.
       ?LET(
          {{{Data, _}, Tx}, ShorterFor},
          {data_sks_and_lock_tx(), non_neg_integer()},
          begin
              Account = get_account(from(Tx), Data),
              {put_account(Account#account{locked_until=Tx#lock_tx.locked_until + ShorterFor}, Data), tx_serialize(Tx)}
          end),
       %% Validity not longer than existing.
       ?LET(
          {{{Data, _}, Tx}, ShorterFor},
          {?SUCHTHAT(
              {{Data, _}, #account_tx{to=ToAddress}},
              data_sks_and_account_tx(),
              get_account(ToAddress, Data) =/= none),
           non_neg_integer()},
          begin
              To = get_account(Tx#account_tx.to, Data),
              {put_account(To#account{valid_until=Tx#account_tx.valid_until + ShorterFor}, Data), tx_serialize(Tx)}
          end),
       %% Account tx: From locked and To other than From.
       ?LET(
          {{{Data, _}, Tx}, LockedFor},
          {?SUCHTHAT(
              {_, #account_tx{from=From, to=To}},
              data_sks_and_account_tx(),
              From =/= To),
           pos_integer()},
          begin
              Account = get_account(from(Tx), Data),
              {put_account(Account#account{locked_until=Data#data.height + LockedFor}, Data), tx_serialize(Tx)}
          end),
       %% Lock tx: Locked until longer than valid until.
       ?LET(
          {{{Data, SKs}, Tx}, LongerFor},
          {data_sks_and_lock_tx(), pos_integer()},
          begin
              #account{valid_until=ValidUntil} = get_account(from(Tx), Data),
              InvalidTx = sign_tx(Tx#lock_tx{locked_until=ValidUntil + LongerFor}, SKs),
              {Data, tx_serialize(InvalidTx)}
          end),
       %% Destination not existing.
       ?LET(
          {Data, ValidTx},
          data_with_transfer_tx(),
          {delete_account(ValidTx#transfer_tx.to, Data), tx_serialize(ValidTx)}),
       %% We could check also destination that will expire in next block, but… that expiration cannot be certain, as account’s validity can be extended after a transaction is delivered.
       %%
       %% From not existing.
       ?LET(
          {Data, ValidTx},
          data_with_transfer_tx(),
          {delete_account(ValidTx#transfer_tx.from, Data), tx_serialize(ValidTx)}),
       %% Transfer tx: From locked.
       ?LET(
          {Data, SKs},
          data_sks(),
          ?LET(
             {From, To},
             {account_from(Data, locked), account_from(Data)},
             ?LET(
                Tx,
                %% We abuse the fact that transfer_tx doesn’t check whether account is locked.
                transfer_tx({Data, SKs}, From, To),
                {Data, tx_serialize(Tx)}))),
       %% Tx not valid yet.
       ?LET(
          {Data, {_, SK}, Tx},
          oneof(
            [fun data_with_account_secret_and_transfer_tx/0,
             fun data_with_account_secret_and_vote_tx/0]),
          ?LET(
             ValidIn,
             pos_integer(),
             {Data, tx_serialize(set_valid_since(Data#data.height + ValidIn, Tx, SK))})),
       %% Tx outdated.
       ?LET(
          {Data, {_, SK}, Tx},
          ?SUCHTHAT(
             {#data{height=Height, epoch_length=EpochLength}, _, _},
             oneof(
               [fun data_with_account_secret_and_transfer_tx/0,
                fun data_with_account_secret_and_vote_tx/0]),
             Height > EpochLength),
          ?LET(
             ExpiredBy,
             pos_integer(),
             {Data, tx_serialize(set_valid_since(Data#data.height - Data#data.epoch_length - ExpiredBy + 1, Tx, SK))})),
       %% Incorrectly specified message length.
       ?LET(
          {{{Data, SKs}, ValidTx}, InvalidLength},
          ?SUCHTHAT(
             {{_, ValidTx}, InvalidLength},
             {data_sks_and_transfer_tx(), choose(0, 255)},
             InvalidLength =/= byte_size(ValidTx#transfer_tx.message)),
          begin
              Message = ValidTx#transfer_tx.message,
              ToSign = to_sign(ValidTx),
              TxHead = binary:part(ToSign, 0, byte_size(ToSign) - byte_size(Message) - 1),
              NewToSign = <<TxHead/binary, InvalidLength, Message/binary>>,
              {Data, sign(NewToSign, maps:get(from(ValidTx), SKs))}
          end),
       %% Replayed transfer tx or vote tx.
       ?LET(
          {Data, Tx},
          oneof(
            [fun data_with_transfer_tx/0,
             fun data_with_vote_tx/0]),
          begin
              TxBin = tx_serialize(Tx),
              #data{fresh_txs=FreshTxs} = Data,
              NewFreshTxs = ?SETS:add_element({get_valid_since(Tx), ?HASH(TxBin)}, FreshTxs),
              {Data#data{fresh_txs=NewFreshTxs}, TxBin}
          end)
       %% TODO: Maybe ensure that no more than one vote tx per validator can be included in one block.
      ]).

data_with_tx() ->
    oneof(
      [fun data_with_transfer_tx/0,
       fun data_with_lock_tx/0,
       fun data_with_account_tx/0,
       fun data_with_vote_tx/0]).

tx() ->
    oneof(
      [fun transfer_tx/0,
       fun valid_lock_tx/0,
       fun account_tx/0,
       fun vote_tx/0]).

%% Properties.

prop_account_serialization() ->
    ?FORALL(A, account(), account_deserialize(account_serialize(A)) =:= A).

prop_tx_serialization() ->
    ?FORALL(Tx, tx(), tx_deserialize(tx_serialize(Tx)) =:= Tx).

prop_account_query_returns_serialized_account() ->
    ?FORALL(
       {Data, Account},
       data_with_account(),
       begin
           #'ResponseQuery'{value=ResponseValue} =
               gossiping(
                 #'RequestQuery'{
                    path="account",
                    data=Account#account.address},
                 Data),
           {_Address, Value} = account_serialize(Account),
           Value =:= ResponseValue
       end).

prop_account_query_returns_not_found_code_for_non_existing_account() ->
    ?FORALL(
       {Data, NonExistingAddress},
       {data(), binary(32)},
       begin
           #'ResponseQuery'{code=ResponseCode} =
               gossiping(
                 #'RequestQuery'{
                    path="account",
                    data=NonExistingAddress},
                 Data),
           ResponseCode =:= ?NOT_FOUND
       end).

prop_unknown_query_returns_bad_request_code() ->
    ?FORALL(
       Data,
       data(),
       begin
           #'ResponseQuery'{code=ResponseCode} =
               gossiping(
                 #'RequestQuery'{
                    path="non-existent",
                    data= <<"foo">>},
                 Data),
           ResponseCode =:= ?BAD_REQUEST
       end).

prop_valid_tx_is_positively_checked() ->
    ?FORALL(
       {Data, ValidTx},
       data_with_valid_tx(),
       begin
           #'ResponseCheckTx'{code=ResponseCode} =
               gossiping(
                 #'RequestCheckTx'{
                    tx=tx_serialize(ValidTx)},
                Data),
           ?OK =:= ResponseCode
       end).

prop_transfer_tx_adds_balance_to_destination() ->
    ?FORALL(
       {Data, Tx=#transfer_tx{from=From, to=To}},
       data_with_transfer_tx(),
       ?IMPLIES(
          From =/= To,
          begin
              {#'ResponseDeliverTx'{code=?OK}, NewData} =
                  committing(
                    #'RequestDeliverTx'{
                       tx=tx_serialize(Tx)},
                    Data),
              To = Tx#transfer_tx.to,
              lookup_balance(To, NewData) =:= lookup_balance(To, Data) + Tx#transfer_tx.value
          end)).

prop_lock_tx_locks_account_or_extends_lock() ->
    ?FORALL(
       {{Data, _}, Tx=#lock_tx{locked_until=NewLockedUntil}},
       data_sks_and_lock_tx(),
       begin
           {#'ResponseDeliverTx'{code=?OK}, NewData} =
               committing(
                 #'RequestDeliverTx'{
                    tx=tx_serialize(Tx)},
                 Data),
           FreshAccount = get_account(from(Tx), NewData),
           FreshAccount#account.locked_until =:= NewLockedUntil
       end).

prop_account_tx_extends_validity() ->
    ?FORALL(
       {{Data, _}, Tx=#account_tx{to=ToAddress}},
       data_sks_and_account_tx(),
       begin
           {#'ResponseDeliverTx'{code=?OK}, NewData} =
               committing(
                 #'RequestDeliverTx'{
                    tx=tx_serialize(Tx)},
                 Data),
           FreshTo = get_account(ToAddress, NewData),
           FreshTo#account.valid_until =:= Tx#account_tx.valid_until
       end).

prop_vote_tx_changes_vote() ->
    ?FORALL(
       {Data=#data{height=Height, epoch_length=EpochLength}, Tx},
       data_with_vote_tx(),
       begin
           {#'ResponseDeliverTx'{code=?OK}, NewData} =
               committing(
                 #'RequestDeliverTx'{
                    tx=tx_serialize(Tx)},
                 Data),
           case gb_merkle_trees:lookup(from(Tx), NewData#data.validators) of
               none ->
                   true;
               _ ->
                   get_vote(from(Tx), NewData) =:= Tx#vote_tx.vote
           end andalso
               case Height rem EpochLength >= EpochLength * 3 div 4 of
                   true ->
                       case gb_merkle_trees:lookup(from(Tx), NewData#data.future_validators) of
                           none ->
                               true;
                           _ ->
                               get_vote(from(Tx), NewData, future_validators) =:= Tx#vote_tx.vote
                       end;
                   false ->
                       true
               end
       end).

prop_tx_puts_fee_into_fee_deposits() ->
    ?FORALL(
       {Data, Tx},
       data_with_tx(),
       begin
           #data{fee_deposit_short=FeeDepositShort, fee_deposit_long=FeeDepositLong} = Data,
           FeeShort = fee_short(Tx, Data),
           FeeLong = fee_long(Tx, Data),
           {#'ResponseDeliverTx'{code=?OK}, #data{fee_deposit_short=NewFeeDepositShort, fee_deposit_long=NewFeeDepositLong}} =
               committing(
                 #'RequestDeliverTx'{
                    tx=tx_serialize(Tx)},
                 Data),
           NewFeeDepositShort =:= FeeDepositShort + FeeShort andalso
               NewFeeDepositLong =:= FeeDepositLong + FeeLong
       end).

prop_tx_does_not_change_money_supply() ->
    ?FORALL(
       {Data, Tx},
       data_with_tx(),
       money_supply(apply_tx(Tx, Data)) =:= money_supply(Data)).

prop_end_block_does_not_change_money_supply() ->
    ?FORALL(
       Data,
       data(),
       money_supply(end_block(Data)) =:= money_supply(Data)).

prop_invalid_tx_is_negatively_checked() ->
    ?FORALL(
       {Data, TxBin},
       data_with_invalid_tx_bin(),
       begin
           #'ResponseCheckTx'{code=ResponseCode} =
               gossiping(
                 #'RequestCheckTx'{
                    tx=TxBin},
                 Data),
           ResponseCode =/= ?OK
       end).

prop_invalid_tx_does_not_modify_data() ->
    ?FORALL(
       {Data, TxBin},
       data_with_invalid_tx_bin(),
       begin
           {#'ResponseDeliverTx'{code=ResponseCode}, NewData} =
               committing(
                 #'RequestDeliverTx'{
                    tx=TxBin},
                 Data),
           ResponseCode =/= ?OK andalso Data =:= NewData
       end).

prop_validators_at_end_block() ->
    ?FORALL(
       Data=#data{epoch_length=EpochLength, height=Height, validators=Validators, future_validators=FutureValidators},
       data(),
       begin
           {keep_state, #data{validators=NewValidators},
            {reply, "from", #'ResponseEndBlock'{validator_updates=Diffs}}} =
               committing({call, "from"}, #'RequestEndBlock'{}, Data),
           NormalizationFun =
               fun (Validators2) ->
                       gb_merkle_trees:foldr(
                         fun ({PK, <<Power, _/binary>>}, Acc) ->
                                 [{PK, Power}|Acc]
                         end,
                         [],
                         Validators2)
               end,
           case (Height + 1) rem EpochLength of
               0 ->
                   NewValidators =:= FutureValidators andalso
                       NormalizationFun(apply_diffs(Diffs, Validators)) =:= NormalizationFun(NewValidators);
               _ ->
                   NewValidators =:= Validators andalso Diffs =:= []
           end
       end).

prop_future_validators_at_end_block() ->
    ?FORALL(
       Data=#data{height=Height, epoch_length=EpochLength, future_validators=FutureValidators},
       data(),
       begin
           #data{validators=NewValidators, future_validators=NewFutureValidators} = end_block(Data),
           Stage = (Height + 1) rem EpochLength,
           if
               Stage =:= 0 ->
                   NewFutureValidators =:= undefined andalso NewValidators =:= FutureValidators;
               Stage < EpochLength div 4 ->
                   NewFutureValidators =:= undefined;
               Stage =:= EpochLength div 4 ->
                   {promise, Key, Hash} = NewFutureValidators,
                   {value, Val} = docile_rpc:nb_yield(Key, 10),
                   Val =:= new_validators(Data) andalso Hash =:= app_hash(Data);
               Stage < EpochLength * 3 div 4 ->
                   {promise, _, _} = NewFutureValidators,
                   true;
               true ->
                   %% If this does not fail, we assume that we are dealing with gb_merkle_tree.
                   _ = gb_merkle_trees:root_hash(NewFutureValidators),
                   true
           end
       end).

prop_new_validators_will_have_accounts_when_epoch_starts() ->
    ?FORALL(
       Data=#data{height=Height, epoch_length=EpochLength},
       data(),
       begin
           NewValidators = new_validators(Data),
           Addresses =
               gb_merkle_trees:foldr(
                 fun ({Address, _}, Acc) -> [Address|Acc] end,
                 [],
                 NewValidators),
           lists:all(
             fun (Address) ->
                     #account{valid_until=ValidUntil} = get_account(Address, Data),
                     ValidUntil >= EpochLength * (Height div EpochLength + 1)
             end,
             Addresses)
       end).

prop_info_responds_with_app_hash_and_height() ->
    ?FORALL(
       Data,
       data(),
       begin
           #'ResponseInfo'{last_block_height=Height, last_block_app_hash=AppHash} =
               gossiping(#'RequestInfo'{}, Data),
           Height =:= height(Data) andalso AppHash =:= app_hash(Data)
       end).

prop_commit_responds_with_app_hash() ->
    ?FORALL(
       Data,
       data(),
       begin
           {next_state, gossiping, NewData, [{reply, "from", #'ResponseCommit'{data=ResponseData}}|_]} =
               committing({call, "from"}, #'RequestCommit'{}, Data),
           ResponseData =:= app_hash(NewData)
       end).

prop_begin_block_increments_absencies() ->
    ?FORALL(
       {Data=#data{validators=Validators}, AbsentValidators, Header},
       ?LET(
          Data=#data{validators=Validators},
          data(),
          {Data,
           ?LET(
              AbsentValidatorsNonUnique,
              list(elements(lists:seq(0, gb_merkle_trees:size(Validators) - 1))),
              uniqify_list(AbsentValidatorsNonUnique)),
           header(Data)}),
       begin
           {next_state, committing, #data{validators=ResponseValidators}, {reply, "from", #'ResponseBeginBlock'{}}} =
               gossiping({call, "from"}, #'RequestBeginBlock'{absent_validators=AbsentValidators, header=Header}, Data),
           ValidatorsList = gb_merkle_trees:to_orddict(Validators),
           ResponseValidatorsList = gb_merkle_trees:to_orddict(ResponseValidators),
           lists:all(
             fun (I) ->
                     {_, <<_, Absencies:3/unit:8, _/binary>>} = lists:nth(I + 1, ValidatorsList),
                     {_, <<_, ResponseAbsencies:3/unit:8, _/binary>>} = lists:nth(I + 1, ResponseValidatorsList),
                     ResponseAbsencies =:= Absencies + 1
             end,
             AbsentValidators)
       end).

prop_begin_block_updates_timestamp() ->
    ?FORALL(
       {Data, Header},
       ?LET(
          Data,
          data(),
          {Data, header(Data)}),
       begin
           {next_state, committing, #data{timestamp=NewTimestamp}, {reply, "from", #'ResponseBeginBlock'{}}} =
               gossiping({call, "from"}, #'RequestBeginBlock'{header=Header}, Data),
           NewTimestamp =:= Header#'Header'.time
       end).

prop_end_block_increments_height() ->
    ?FORALL(
       Data,
       data(),
       height(end_block(Data)) =:= height(Data) + 1).

prop_end_block_expires_accounts_and_puts_balances_into_long_deposit() ->
    ?FORALL(
       {Data, Account},
       data_with_account(),
       begin
           ExpiringAccount = Account#account{valid_until=Data#data.height},
           ExpiringData = put_account(ExpiringAccount, Data),
           NewData = end_block(ExpiringData),
           get_account(Account#account.address, NewData) =:= none andalso
               NewData#data.fee_deposit_long >= Account#account.balance
       end).

%% TODO: Give this more attention, ensure that the right thing is tested.
prop_after_end_block_all_validators_have_accounts() ->
    ?FORALL(
       {Data, Account},
       data_with_account(),
       begin
           ExpiringAccount = Account#account{valid_until=Data#data.height},
           ExpiringData = put_account(ExpiringAccount, Data),
           NewData = end_block(ExpiringData),
           get_account(Account#account.address, NewData) =:= none
       end).

prop_end_block_does_not_expire_valid_accounts() ->
    ?FORALL(
       {{Data, Account}, ValidFor},
       {data_with_account(), pos_integer()},
       begin
           NonExpiringAccount = Account#account{valid_until=Data#data.height + ValidFor},
           NonExpiringData = put_account(NonExpiringAccount, Data),
           NewData = end_block(NonExpiringData),
           get_account(Account#account.address, NewData) =/= none
       end).

prop_fee_deposit_granting() ->
    ?FORALL(
       Data=#data{fee_deposit_short=FeeDepositShort, fee_deposit_long=FeeDepositLong, height=Height, epoch_length=EpochLength, validators=Validators},
       data(),
       begin
           NewData = #data{fee_deposit_short=NewFeeDepositShort, fee_deposit_long=NewFeeDepositLong} = end_block(Data),
           case Height rem EpochLength of
               0 ->
                   ExpectedDisposedDeposit = FeeDepositShort + FeeDepositLong div ?FEE_DEPOSIT_LONG_RATIO,
                   case ExpectedDisposedDeposit of
                       0 ->
                           NewFeeDepositShort =:= FeeDepositShort andalso NewFeeDepositLong >= FeeDepositLong;
                       _ ->
                           %% If a validator has at least 1/3 of absencies, it should not get any reward.
                           SharesValidators = [{VP, PK} || {PK, <<VP, Absencies:3/unit:8, _/binary>>} <- gb_merkle_trees:to_orddict(Validators), Absencies < EpochLength div 3],
                           ValidatorsExpectedRewards = 'hare-niemeyer':apportion(SharesValidators, ExpectedDisposedDeposit),
                           NewFeeDepositShort =:= 0 andalso NewFeeDepositLong >= FeeDepositLong - FeeDepositLong div ?FEE_DEPOSIT_LONG_RATIO andalso
                               lists:all(
                                 fun ({PK, Reward}) ->
                                         case get_account(PK, NewData) of
                                             #account{balance=BalanceNew} ->
                                                 #account{balance=BalanceOld} = get_account(PK, Data),
                                                 BalanceNew - BalanceOld =:= Reward;
                                             none ->
                                                 true
                                         end
                                 end,
                                 ValidatorsExpectedRewards)
                   end;
               _ ->
                   NewFeeDepositShort =:= FeeDepositShort andalso NewFeeDepositLong >= FeeDepositLong
           end
       end).

prop_end_block_unlocks_some_accounts() ->
    ?FORALL(
       {{Data=#data{height=Height}, _}, Account=#account{locked_until=LockedUntil, valid_until=ValidUntil}},
       data_sks_and_account(locked),
       ?IMPLIES(
          ValidUntil > Height,
          begin
              NewData = end_block(Data),
              #account{locked_until=NewLockedUntil} = get_account(Account#account.address, NewData),
              NewLockedUntil =:=
                  case LockedUntil of
                      Height ->
                          none;
                      _ ->
                          LockedUntil
                  end
          end)).

prop_end_block_truncates_fresh_txs() ->
    ?FORALL(
       Data,
       data(),
       begin
           FreshTxs = Data#data.fresh_txs,
           #data{fresh_txs=NewFreshTxs, height=NewHeight} = end_block(Data),
           NewFreshTxs =:= ?SETS:filter(fun ({ValidSince, _Tx}) -> ValidSince >= NewHeight - Data#data.epoch_length + 2 end, FreshTxs)
       end).

prop_transfer_tx_and_vote_tx_are_added_to_fresh_txs_and_fresh_txs_hash() ->
    ?FORALL(
       {Data, Tx},
       oneof(
         [fun data_with_transfer_tx/0,
          fun data_with_vote_tx/0]),
       begin
           #data{fresh_txs_hash=FreshTxsHash} = Data,
           TxBin = tx_serialize(Tx),
           {keep_state,
            #data{fresh_txs=NewFreshTxs, fresh_txs_hash=NewFreshTxsHash},
            {reply, "from", #'ResponseDeliverTx'{code=?OK}}} =
               committing({call, "from"}, #'RequestDeliverTx'{tx=TxBin}, Data),
           ?SETS:is_element({get_valid_since(Tx), ?HASH(TxBin)}, NewFreshTxs) andalso
               NewFreshTxsHash =:= ?HASH(<<FreshTxsHash/binary, TxBin/binary>>)
       end).
-endif.
