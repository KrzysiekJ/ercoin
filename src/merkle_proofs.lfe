(defmodule merkle_proofs
  (export (deserialize 1)
          (serialize 1)
          (fold 1)))

(deftype merkle-path (list (tuple (UNION 'left 'right) (binary))))
(deftype merkle-proof (tuple (binary) (merkle-path)))

(defspec (deserialize 1)
  (((binary)) (UNION (merkle-proof) 'none)))
(defun deserialize
  (((binary (leaf binary (size 32)) (path-bin binary)))
   (case (deserialize-path path-bin)
     ('none 'none)
     ((list) 'none)
     (path (tuple leaf path))))
  ((_) 'none))

(defspec (deserialize-path 1)
  (((bitstring)) (UNION (merkle-path) 'none)))
(defun deserialize-path
  ((padding) (when (< (bit_size padding) 8))
   (let ((padding-size (bit_size padding)))
     (case padding
       ((binary (0 (size padding-size))) (list))
       (_ 'none))))
  (((binary (direction-int (size 1)) (step binary (size 32)) (rest-bits bitstring)))
   (case (deserialize-path rest-bits)
     ('none 'none)
     (rest (cons
            (tuple
             (case direction-int
               (1 'right)
               (0 'left))
             step)
            rest))))
  ((_)
   'none))

(defspec (serialize 1)
  (((merkle-proof)) (binary)))
(defun serialize
  (((tuple leaf path))
   (let* ((path-bin (serialize-path path))
          (padding-size (rem (- 8 (rem (bit_size path-bin) 8)) 8)))
     (binary (leaf binary) (path-bin bitstring) (0 (size padding-size))))))

(defspec (serialize-path 1) (((merkle-path)) (bitstring)))
(defun serialize-path
  (((list))
   (binary))
  (((cons (tuple direction hash) steps))
   (let ((steps-bin (serialize-path steps))
         (direction-bit (case direction
                          ('left 0)
                          ('right 1))))
     (binary (direction-bit (size 1)) (hash binary) (steps-bin bitstring)))))

(defspec (fold 1)
  (((merkle-proof)) (binary)))
(defun fold
  "Compute root hash from a Merkle proof."
  (((tuple val (list)))
   val)
  (((tuple val (cons (tuple direction step-val) steps)))
   (let ((to-hash (case direction
                    ('left (binary (step-val binary) (val binary)))
                    ('right (binary (val binary) (step-val binary))))))
     (fold (tuple (crypto:hash 'sha256 to-hash) steps)))))

(defmodule merkle_proofs_test
  (export all))

(include-lib "triq/include/triq.hrl")

(defun merkle-path ()
  (non_empty (triq_dom:list (tuple (oneof '(left right)) (triq_dom:binary 32)))))

(defun merkle-proof ()
  (tuple (triq_dom:binary 32) (merkle-path)))

(defun padding-size (proof-bin)
  (rem (- (bit_size proof-bin) 256) 257))

(defun prop_proof_serialization ()
  (FORALL
   proof
   (merkle-proof)
   (=:= (clj:-> proof
            (merkle_proofs:serialize)
            (merkle_proofs:deserialize))
        proof)))

(defun prop_proof_serialization_size ()
  "Serialized proofs do not contain incomplete bytes."
  (FORALL
   proof
   (merkle-proof)
   (=:= 0 (rem (bit_size (merkle_proofs:serialize proof)) 8))))

(defun prop_deserialization_of_binary_with_wrong_size_returns_none ()
  (FORALL
   rand-bin
   (triq_dom:binary)
   (IMPLIES
    (orelse
     ;; By including equality here, we forbid proofs with empty paths, as they are unnecessary and may be a source of transaction malleability.
     (=< (bit_size rand-bin) 256)
     (> (padding-size rand-bin) 8))
    (=:= 'none (merkle_proofs:deserialize rand-bin)))))

(defun prop_deserialization_with_non_zero_padding_returns_none ()
  (FORALL
   (tuple _ invalid-proof-bin)
   (SUCHTHAT
    (tuple proof-bin invalid-proof-bin)
    (LET
     (tuple proof new-padding)
     (tuple (merkle-proof) (choose 0 255))
     (let* ((proof-bin (merkle_proofs:serialize proof))
            (padding-size (padding-size proof-bin))
            (raw-proof-bin-size (- (bit_size proof-bin) padding-size))
            ((binary (raw-proof-bin bitstring (size raw-proof-bin-size)) (_ bitstring)) proof-bin))
       (tuple proof-bin
              (binary (raw-proof-bin bitstring)
                      (new-padding (size padding-size))))))
    (=/= proof-bin invalid-proof-bin))
   (=:= 'none (merkle_proofs:deserialize invalid-proof-bin))))
