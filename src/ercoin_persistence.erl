-module(ercoin_persistence).
-behaviour(gen_server).

%% API.
-export([start_link/0]).
-export([current_data/0]).
-export([dump_data_async/1]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-record(state, {
}).

%% API.

-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

data_dir() ->
    case os:getenv("ERCOIN_HOME") of
        false ->
            Home = os:getenv("HOME"),
            filename:absname_join(Home, ".ercoin");
        ErcoinHome ->
            ErcoinHome
    end.

-spec current_data() -> term().
current_data() ->
    DataPath = filename:absname_join(data_dir(), "current_data.bin"),
    {ok, DataBin} = file:read_file(DataPath),
    binary_to_term(DataBin).

%% @doc Dump data asynchronously.
%% This function converts term to binary and schedules writing it to disc.
-spec dump_data_async(term()) -> ok.
dump_data_async(Data) ->
    %% We create a binary here to not copy large term between processes.
    DataBin = term_to_binary(Data),
    gen_server:cast(?MODULE, {store_data_bin, DataBin}).

%% gen_server.

init([]) ->
    {ok, #state{}}.

handle_call(_Request, _From, State) ->
    {reply, ignored, State}.

handle_cast({store_data_bin, DataBin}, State) ->
    DataPath = filename:absname_join(data_dir(), "current_data.bin"),
    ok = write_atomically(DataPath, DataBin),
    {noreply, State};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

-spec write_atomically(file:name_all(), binary()) -> ok | {error, term()}.
%% @doc Write file atomically, with obtaining lock.
write_atomically(Filename, Bytes) ->
    TmpFilename = [Filename, ".tmp"],
    case file:open(TmpFilename, [exclusive]) of
        {ok, F} ->
            ok = file:write(F, Bytes),
            file:rename(TmpFilename, Filename);
        {error, eexist} ->
            {error, temporary_file_already_exists}
    end.
