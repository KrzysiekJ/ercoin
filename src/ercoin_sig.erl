%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_sig).

-export(
   [verify_detached/3]).

-spec verify_detached(Sig :: binary(), Msg :: binary(), Address :: binary()) -> boolean().
verify_detached(<<RawSignature:64/binary, ProofBin/binary>>, Msg, Address) ->
    case ProofBin of
        <<>> ->
            verify_ed25519(RawSignature, Msg, Address);
        _ ->
            case merkle_proofs:deserialize(ProofBin) of
                {PK, Path} ->
                    case merkle_proofs:fold({PK, Path}) of
                        Address ->
                            verify_ed25519(RawSignature, Msg, PK);
                        _ ->
                            false
                    end;
                none ->
                    false
            end
    end;
verify_detached(_, _, _) ->
    false.

-spec verify_ed25519(Sig :: binary(), Msg :: binary(), PK :: binary()) -> boolean().
verify_ed25519(Signature, Msg, PK) ->
    case libsodium_crypto_sign_ed25519:verify_detached(Signature, Msg, PK) of
        0 ->
            true;
        -1 ->
            false
    end.
