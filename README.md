# Ercoin

Ercoin is a proof of stake cryptocurrency written in [Erlang](https://www.erlang.org) using [Tendermint](https://tendermint.com) and released under Apache License 2.0. Currently in development phase; some features below may be unimplemented yet and are subject to change.

[Issues are tracked on GitLab.](https://gitlab.com/Ercoin/ercoin/issues) Contributions are welcome. Before submitting a patch, read [contributing guidelines](CONTRIBUTING.md).

For support and other ephemeral discussions, see [the #ercoin IRC channel on irc.freenode.net](irc://irc.freenode.net/ercoin).

## Features

* 100% proof of stake — no wasteful mining.
* Transaction messages.
* 1-second confirmations.
* No chain forks.
* Expirable accounts instead of unspent transaction outputs.
* Fair distribution via proof of burn — no premine, no ICO.
* Accounts managed by multiple keys in arbitrary combinations.

## Development installation

1. Install [Tendermint](https://tendermint.com) (version 0.15.x).
2. Install [Erlang](https://www.erlang.org) (19 is the minimum version).
3. Install [IPFS](https://ipfs.io/) and run `ipfs init`.
4. Install [Sodium](https://download.libsodium.org/doc/).
5. Clone the Ercoin’s repository and enter the created directory.
6. `./bootstrap.sh`.
7. `make run`.
8. In another window, run `tendermint node --home ~/.ercoin/`.

You can play with the node using [`ercoin_wallet`](https://gitlab.com/Ercoin/ercoin_wallet).

## Technical specification

All numbers are big endian.

Keys are Ed25519.

### Transfer transaction format

* Transaction type (0, 1 byte).
* Valid since (4 bytes) — earliest block height at which tx can be included.
* From address (32 bytes).
* To address (32 bytes).
* Value (8 bytes).
* Message length (1 byte).
* Message.
* Signature of all the previous fields.

Fee: per tx + per byte

### Account transaction format

* Transaction type (1, 1 byte).
* Valid until block (4 bytes).
* From address (32 bytes).
* To address (32 bytes).
* Signature of all the previous fields.

Fee: Per tx + per byte + per blocks.

### Lock transaction format

* Transaction type (2, 1 byte).
* Locked until block (4 bytes).
* Address (32 bytes).
* Signature of all the previous fields.

Fee: per tx + per byte + per blocks.

### Vote transaction format

* Transaction type (3, 1 byte).
* Valid since (4 bytes) — earliest block height at which tx can be included.
* Address (32 bytes).
* Votes.
* Signature of all the previous fields.

Fee: none

### Votes format

* Price per tx (8 bytes).
* Price per 256 bytes per tx (8 bytes).
* Price of storing an account for 1 day (8 bytes).
* Protocol version (1 byte).

### Signature format

* An Ed25519 signature (64 bytes).
* Optional Merkle proof that the public key is contained in the address.

#### Merkle proof format

* Public key
* Non-empty sequence of the following:
  * 1 bit indicating whether the following value needs to be appended (in case of true) or prepended (in case of false) to obtain next hash.
  * Value contributing to the hash.

The proof is padded from the right by zero bits to form full bytes.

### Account balances

Key:
* public key (32 bytes).

Value:

* Valid until (4 bytes).
* Balance (8 bytes).
* Locked until (optional, 4 bytes). May not be higher than “valid until”.

### Error codes

* 0 — ok.
* 1 — bad request.
* 2 — not found.
* 3 — forbidden.
* 4 — insufficient funds.
* 5 — invalid timestamp.
* 6 — already exeuted.

### Application hash

The following Merkle tree is formed when calculating an application hash:

* Accounts’ root hash.
* Other data hash:
  * Protocol number.
  * Block height.
  * POSIX timestamp of last block in miliseconds.
  * Short fee deposit.
  * Long fee deposit.
  * Validators’ hash.
  * Future validators’ hash.
  * Fresh transactions’ hash.

### State machine mechanism

Blocks are divided into epochs, during which validator set is almost immutable.

Validators are responsible for providing replay protection.

Transfer transactions modify accounts’ balances. They may not be performed from a locked account, but they can transfer funds to a locked account.

Lock transactions lock accounts and declare them as ready to become a validator. They may be performed by locked accounts.

Vote transactions can be performed by validators to vote for fee sizes and protocol changes.

Account transaction create accounts or extend their validity.

Every transaction except a vote transaction deducts a fee from the account from which it is performed. Fees are calculated from validators’ votes. They are put into long fee deposit (fees per block) and short fee deposit (fees per tx and per byte).

At the end of every epoch, short fee deposit and part of long fee deposit are divided between those validators that have less than 2/3 of absencies in block signatures, proportionally to their voting power.

When new epoch is reached, validator set is replaced by a new one which has been drawn previously. When quarter of epoch (rounded down) is reached, data is locked for the purpose of drawing new validators. When three quarters of epoch (rounded down) are reached, result of drawing is yielded into application state. From that point future validators are able to send fee votes.

Drawing of validators is performed randomly among locked accounts, with handicap proportional to:

* account balance;
* time for which an account will be locked, counting from the start of the next epoch.

If a validator proposes an invalid tx in a block, it pays a fee like it was a transaction tx.

If balance of a validator reaches 0, it is removed from the validator set.

## Initial distribution

Initial amount of coins will be distributed to addresses proportionally to the associated amounts of [BlackCoin](https://blackcoin.co) burnt in a selected time window using [`OP_RETURN`](https://en.bitcoin.it/wiki/OP_RETURN).

## License

This software is licensed under under [the Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) (the “License”); you may not use this software except in compliance with the License. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
